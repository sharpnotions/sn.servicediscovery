function Replace-Assembly-Version {
    param(
        $file,
        [String] $version
    )
    $arrayContents = Get-Content $file |
        % { $_ -replace "Version\(""[^""]*""\)", "Version(""$version"")" }

    [string]::Join("`n", $arrayContents) | Set-Content ($file.FullName)
}

function Replace-Content {
    param(
        $file,
        [String] $version
    )
    #replace the string contents of the nuspec file
    $arrayContents = Get-Content $file |
        % { $_ -replace "0\.0\.0\.0",$version } |
        % { $_ -replace "Copyright \?", ("Copyright "+$company) }
    
    #write it back out to the nuspec file
    [string]::Join("`n", $arrayContents) | Set-Content ($file.FullName)
}

Task Default -depends Build

Task Build {
    $version = Read-Host -Prompt "Assembly Version [1.0.0.0]"
    if($version -eq '') {
        $version = "1.0.0.0"
    }

    Replace-Assembly-Version (Get-Item src\CommonAssemblyInfo.cs) $version

    Exec { msbuild "SN.ServiceDiscovery.sln" /t:Build /p:Configuration=Release }
}

Task Nuget -depends Build {
    
    $nugetVersion = Read-Host -Prompt "Nuget Package Version [1.0.0]"
    if($nugetVersion -eq '') {
        $nugetVersion = "1.0.0"
    }
    
    if(Test-Path("nuget-build")){
        rmdir nuget-build -Recurse
    }
    mkdir nuget-build
    
    Get-Item nuget-specs\*.nuspec | % { Copy-Item $_ nuget-build\ }
    
    #perform the content replacement
    Get-Item nuget-build\*.nuspec | % { Replace-Content $_ $nugetVersion }
    
    Get-Item nuget-build\*.nuspec |
        % { .\.nuget\nuget.exe pack ($_.FullName) -BasePath nuget-build\ -OutputDirectory nuget-build\ }

}