﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace SN.ServiceDiscovery.Plugins
{
    /// <summary>
    /// A type mapping plugin for the SN.ServiceDiscovery assembly which utilized AutoMapper
    /// to perform type conversions.
    /// </summary>
    public class ServiceAutomapper : IServiceTypeMapper
    {
        private IMapper mapper;

        /// <summary>
        /// Create a new <see cref="ServiceAutomapper"/> with a given configured mapper instance
        /// </summary>
        /// <param name="mapper">The configured <see cref="IMapper"/> instance to use when mapping types.</param>
        public ServiceAutomapper(IMapper mapper)
        {
            this.mapper = mapper;
        }

        /// <summary>
        /// Map the source type to another type.
        /// </summary>
        /// <typeparam name="T">The type to map to</typeparam>
        /// <param name="source">The source object to map</param>
        /// <returns>An instance of the target mapped type</returns>
        public T Map<T>(object source)
        {
            if (source == null) throw new ArgumentNullException("source");
            return mapper.Map<T>(source);
        }

        /// <summary>
        /// Map the source type to another type.
        /// </summary>
        /// <param name="source">The source object to map</param>
        /// <param name="target">The type to map to</param>
        /// <returns>An instance of the target mapped type</returns>
        public object Map(object source, Type target)
        {
            if (source == null) throw new ArgumentNullException("source");
            return mapper.Map(source, source.GetType(), target);
        }
    }
}
