﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// A service discovery engine, used to find and document potential service endpoints
    /// given a set of class types.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The discovery core is meant to be used as the first step in service discovery. It
    /// will enumerate the given types, selecting service implementations, and tracing
    /// through the interface inheritance to determine which methods can be exported via
    /// some other mechanism to create a public api.
    /// </para>
    /// <para>
    /// In the process of doing so, the DiscoveryCore will generate an amount of metadata
    /// allowing it to do its job. This metadata can then be used to generate endpoint code
    /// or API documentation.
    /// </para>
    /// </remarks>
    public class DiscoveryCore
    {
        /// <summary>
        /// Create a new <see cref="DiscoveryCore"/> with a default configuration, then perform API discovery.
        /// </summary>
        /// <param name="types">The types to inspect for services</param>
        public DiscoveryCore(params Type[] types) : this(new DiscoveryConfiguration(), types) { }

        /// <summary>
        /// Create a new <see cref="DiscoveryCore"/> with the specified configuration, then perform API discovery.
        /// </summary>
        /// <param name="config">The discovery configuration to use</param>
        /// <param name="types">The types to inspect for services</param>
        public DiscoveryCore(DiscoveryConfiguration config, params Type[] types)
        {
            Config = config;
            Services = types
                .Where(t => t.IsClass
                            && !t.IsAbstract
                            && t.GetCustomAttributes(true).Any(a => a is ApiImplementationAttribute))
                .Select(t => new ServiceMetadata(t))
                .ToArray();
        }

        /// <summary>
        /// The configuration object to use for API discovery and generation
        /// </summary>
        public DiscoveryConfiguration Config { get; private set; }

        /// <summary>
        /// The collection of service metadata that has been collected by service discovery
        /// </summary>
        public IEnumerable<ServiceMetadata> Services { get; private set; }
    }
}
