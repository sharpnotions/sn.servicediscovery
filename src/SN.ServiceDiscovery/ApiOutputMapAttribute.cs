﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Place on contract types to indicate that the automatic api facilities should map
    /// instances of that type to a different type of object.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Enum|AttributeTargets.Struct)]
    public class ApiOutputMapAttribute : Attribute
    {
        /// <summary>
        /// Map instances of this type to another type before output.
        /// </summary>
        /// <param name="resultType">The type that should be mapped to.</param>
        public ApiOutputMapAttribute(Type resultType)
        {
            ResultType = resultType;
        }

        /// <summary>
        /// Gets or sets the result type for instances of the annotated type.
        /// </summary>
        /// <value>
        /// The type to map the result to.
        /// </value>
        public Type ResultType { get; set; }
    }
}