﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// The type of HTTP request a method should respond to.
    /// </summary>
    public enum WebApiRequestType
    {
        /// <summary>Respond to a HTTP GET reqeust</summary>
        GET,
        /// <summary>Respond to a HTTP POST request</summary>
        POST,
        /// <summary>Respond to a HTTP PUT request</summary>
        PUT,
        /// <summary>Respond to a HTTP DELETE request</summary>
        DELETE
    }
}
