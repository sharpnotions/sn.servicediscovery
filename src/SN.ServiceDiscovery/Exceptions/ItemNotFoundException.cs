﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Exceptions
{
    /// <summary>
    /// Indicates that the requested item could not be found by the API
    /// </summary>
    public class ItemNotFoundException : Exception
    {
        /// <summary>
        /// Create a new <see cref="ItemNotFoundException"/>.
        /// </summary>
        public ItemNotFoundException() : base() { }

        /// <summary>
        /// Create a new <see cref="ItemNotFoundException"/> with the specified message.
        /// </summary>
        /// <param name="message"></param>
        public ItemNotFoundException(string message) : base(message) { }
    }
}
