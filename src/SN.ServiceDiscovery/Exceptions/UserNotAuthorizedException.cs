﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Exceptions
{
    /// <summary>
    /// This exception indicates that the invoked API requires a specific privilege,
    /// which the authenticated user did not posess at the time of invocation.
    /// </summary>
    public class UserNotAuthorizedException : Exception
    {
        /// <summary>
        /// Creates a <see cref="UserNotAuthorizedException"/>.
        /// </summary>
        public UserNotAuthorizedException() : base() { }

        /// <summary>
        /// Creates a <see cref="UserNotAuthorizedException"/> with the specified message.
        /// </summary>
        /// <param name="message"></param>
        public UserNotAuthorizedException(string message) : base(message) { }
    }
}
