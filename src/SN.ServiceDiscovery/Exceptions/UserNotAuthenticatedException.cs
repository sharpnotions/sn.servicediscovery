﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Exceptions
{
    /// <summary>
    /// An exception indicating that the user must be authenticated to access the requested API
    /// </summary>
    public class UserNotAuthenticatedException : Exception
    {
        /// <summary>
        /// Create a new <see cref="UserNotAuthenticatedException"/> with the default message.
        /// </summary>
        public UserNotAuthenticatedException() : base() { }

        /// <summary>
        /// Create a new <see cref="UserNotAuthenticatedException"/> with the specified message.
        /// </summary>
        /// <param name="message">The message for the encapsulating software.</param>
        public UserNotAuthenticatedException(string message) : base(message) { }
    }
}
