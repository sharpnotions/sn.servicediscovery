﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Exceptions
{
    /// <summary>
    /// Indicates that an error occurred during validation.
    /// </summary>
    public class ValidationException : Exception
    {
        /// <summary>
        /// Create a new <see cref="ValidationException"/>
        /// </summary>
        public ValidationException() : base() { }

        /// <summary>
        /// Create a new <see cref="ValidationException"/> for the speified model Type.
        /// </summary>
        /// <param name="modelType">The type of the model which generated the validation error.</param>
        public ValidationException(Type modelType)
            : base(string.Format("Validation error for Type {0}", modelType))
        {
            ModelType = modelType;
        }

        /// <summary>
        /// The <see cref="Type"/> of the Model which generated the validation error.
        /// </summary>
        public Type ModelType { get; private set; }
    }
}
