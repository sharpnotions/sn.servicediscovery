﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Metadata for a discovered service and all of its endpoints.
    /// </summary>
    public class ServiceMetadata
    {
        private static readonly char[] PATH_SEPARATORS = new char[] { '/' };

        /// <summary>
        /// Create a new <see cref="ServiceMetadata"/> object.
        /// </summary>
        /// <param name="implementationType"></param>
        public ServiceMetadata(Type implementationType)
        {
            // initialize the Endpoints collection to ensure it has something
            Endpoints = new ServiceEndpointMetadata[0];

            ImplementationType = implementationType;

            // generate a nicer representation of the full type name than Type.Fullname
            ImplementationClassName = string.Format("{0}.{1}", implementationType.Namespace, implementationType.Name);

            // interrogate the interfaces to find out which one provides the path,
            // throwing an error if there is not a single interface with the necessary attribute
            var pathInterfaces = implementationType.GetInterfaces().Where(i => i.GetCustomAttributes<ApiDefinitionAttribute>().Any()).ToArray();
            if (pathInterfaces.Length != 1)
            {
                IsValid = false;
                ReasonInvalid = string.Format("Implementation type {0} has {1} interfaces with {2}.\nInterfaces:\n\t{3}",
                    ImplementationClassName,
                    pathInterfaces.Length,
                    typeof(ApiDefinitionAttribute).Name,
                    string.Join("\n\t", pathInterfaces.Select(pi => pi.FullName)));
                return;
            }

            //Expose the main definition interface
            ApiDefinitionInterface = pathInterfaces.Single();

            //consume the ApiDefinitionAttribute
            ReadDefinitionAttribute(ApiDefinitionInterface.GetCustomAttribute<ApiDefinitionAttribute>());

            if (IsValid)
            {
                //restrict the interface list down to just Api Definitions
                AbstractInterfaces = implementationType.GetInterfaces()
                    .Where(i => i.GetCustomAttributes(true).Any(a => a is AbstractApiDefinitionAttribute))
                    .ToArray();

                var meh = AbstractInterfaces.Select(i => new
                {
                    Interface = i,
                    Methods = i.GetMethods()
                        .Where(m => m.GetCustomAttributes(true).Any(a => a is ApiMethodAttribute))
                        .ToArray()
                });


                Endpoints = AbstractInterfaces.SelectMany(i =>
                    i.GetMethods()
                        .Where(m => m.GetCustomAttributes(true).Any(a => a is ApiMethodAttribute))
                        .Select(m=> new ServiceEndpointMetadata(i, m))
                    ).ToArray();

                //map the discovered metatdata into the endpoints collection
                //Endpoints = definedMethods.Select(entry => new ServiceEndpointMetadata(entry.Value, entry.Key)).ToArray();
            }
        }

        private void ReadDefinitionAttribute(ApiDefinitionAttribute attribute)
        {
            ServiceName = attribute.Name ?? ImplementationClassName;
            ServicePath = string.Join("/", (attribute.Path ?? string.Empty).Split(PATH_SEPARATORS, StringSplitOptions.RemoveEmptyEntries));
            if (string.IsNullOrWhiteSpace(ServicePath))
            {
                IsValid = false;
                ReasonInvalid = string.Format("Implementation type {0} did not have a valid path: {1}", ImplementationClassName, attribute.Path);
            }
            else
            {
                IsAuthenticationRequired = attribute.AuthenticatedOnly;
                RequiredPrivilege = attribute.RequiredPrivilege;

                //if a privilege is specified it overrides AuthenticatedOnly=false
                IsAuthenticationRequired |= !string.IsNullOrWhiteSpace(RequiredPrivilege);

                IsValid = true;
            }
        }

        /// <summary>
        /// The type that implements the api interfaces
        /// </summary>
        public Type ImplementationType { get; private set; }

        /// <summary>
        /// The main interface that defines the API path and other characteristics
        /// </summary>
        public Type ApiDefinitionInterface { get; private set; }

        /// <summary>
        /// Get the list of interfaces used to define this API
        /// </summary>
        public Type[] AbstractInterfaces { get; private set; }

        /// <summary>
        /// Get the name of the class that implements the API
        /// </summary>
        public string ImplementationClassName { get; private set; }

        /// <summary>
        /// Get the friendly name of the service specified by the attribute
        /// </summary>
        public string ServiceName { get; private set; }

        /// <summary>
        /// Returns true if the service definition and path could be discovered successfully
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// Returns the textual reason the service is not considered valid
        /// </summary>
        public string ReasonInvalid { get; private set; }

        /// <summary>
        /// Get the path for the service 
        /// </summary>
        public string ServicePath { get; private set; }

        /// <summary>
        /// Returns true if authentication is required for this api
        /// </summary>
        public bool IsAuthenticationRequired { get; private set; }

        /// <summary>
        /// If non-null, specifies a privilege ID that is required to access this api
        /// </summary>
        public string RequiredPrivilege { get; private set; }

        /// <summary>
        /// The list of Endpoints discovered for this API
        /// </summary>
        public IEnumerable<ServiceEndpointMetadata> Endpoints { get; private set; }
    }
}
