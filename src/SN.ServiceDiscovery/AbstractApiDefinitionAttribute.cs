﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// This attribute indicates that the specified interface contains API methods, but is not inherently a public facing API
    /// </summary>
    /// <remarks>
    /// This attribute allows the definiton of an interface containing API methods that will be exposed through multiple other
    /// public interfaces.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public class AbstractApiDefinitionAttribute : Attribute
    {
        /// <summary>
        /// The name of the API. Overrides the use of the classname.
        /// </summary>
        public string Name { get; set; }
    }
}