﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Describes a parameter for an API endpoint
    /// </summary>
    public class ServiceParameterMetadata
    {
        /// <summary>
        /// Creates a new <see cref="ServiceParameterMetadata"/> object from a <see cref="ParameterInfo"/> object.
        /// </summary>
        /// <param name="paramInfo">The parameter info object that describes this parameter</param>
        public ServiceParameterMetadata(ParameterInfo paramInfo)
        {
            ParameterName = paramInfo.Name;
            ParameterType = paramInfo.ParameterType;
            IsOptional = paramInfo.IsOptional;
            if (IsOptional)
            {
                OptionalParameterDefaultValue = paramInfo.DefaultValue;
            }
        }

        /// <summary>
        /// Creates a new <see cref="ServiceParameterMetadata"/> object from the combination of the <see cref="ParameterInfo"/>
        /// and <see cref="ApiParameterAttribute"/> objects.
        /// </summary>
        /// <param name="paramInfo">The parameter info object that describes the parameter</param>
        /// <param name="apiAttribute">The api parameter attribute that instructs how the parameter is used</param>
        public ServiceParameterMetadata(ParameterInfo paramInfo, ApiParameterAttribute apiAttribute)
            : this(paramInfo)
        {
            IsUrlBound = apiAttribute.IsUrlParameter;
            ApiRequestType = apiAttribute.ApiRequestType;
        }

        /// <summary>
        /// The name of the API method
        /// </summary>
        public string ParameterName { get; private set; }

        /// <summary>
        /// The type required by the API method's parameter
        /// </summary>
        public Type ParameterType { get; private set; }

        /// <summary>
        /// Returns true if the parameter is bound to a URL parameter
        /// </summary>
        public bool IsUrlBound { get; private set; }

        /// <summary>
        /// Returns true if the parameter is optional
        /// </summary>
        public bool IsOptional { get; private set; }

        /// <summary>
        /// If the parameter is optional, this contains the default value
        /// </summary>
        public object OptionalParameterDefaultValue { get; private set; }

        /// <summary>
        /// If not null, overrides the type to be bound from the public API before being
        /// converted to the <seealso cref="ParameterType"/> needed by the API metho
        /// </summary>
        public Type ApiRequestType { get; private set; }
    }
}
