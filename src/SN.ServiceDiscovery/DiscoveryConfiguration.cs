﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Configuration options for the <seealso cref="DiscoveryCore"/> class.
    /// </summary>
    public class DiscoveryConfiguration
    {
        /// <summary>
        /// Creates a new instance of the <see cref="DiscoveryConfiguration"/> class.
        /// </summary>
        public DiscoveryConfiguration() { }

        /// <summary>
        /// Creates a new instance of the <see cref="DiscoveryConfiguration"/> class with the specified options.
        /// </summary>
        /// <param name="activator">The activator to use.</param>
        /// <param name="mapper">The mapper to use</param>
        /// <param name="parameterProviders">Any parameter providers to use</param>
        public DiscoveryConfiguration(IServiceActivator activator, IServiceTypeMapper mapper, IEnumerable<IServiceActivatorParameterProvider> parameterProviders)
        {
            this.ServiceActivator = activator;
            this.TypeMapper = mapper;
            this.ActivationParameterProviders = (parameterProviders ?? new IServiceActivatorParameterProvider[0]).ToArray();
        }

        /// <summary>
        /// Defines the <see cref="IServiceTypeMapper"/> that will be used to convert types between
        /// their public API Type and the Type defined by the method parameter.
        /// </summary>
        public IServiceTypeMapper TypeMapper { get; set;}

        /// <summary>
        /// Defines the <see cref="IServiceActivator"/> implementation that will be used to activate
        /// working instances of the API services at runtime.
        /// </summary>
        public IServiceActivator ServiceActivator { get; set; }

        /// <summary>
        /// The parameter providers to use from within the generator.
        /// </summary>
        public IServiceActivatorParameterProvider[] ActivationParameterProviders { get; set; }
    }
}
