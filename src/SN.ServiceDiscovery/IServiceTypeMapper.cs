﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Defines a type mapper for changing service parameters from one type to another
    /// </summary>
    public interface IServiceTypeMapper
    {
        /// <summary>
        /// Converts the given source object to the specified destination object
        /// </summary>
        /// <typeparam name="T">The destination type</typeparam>
        /// <param name="source">The source object</param>
        /// <returns>The converted object</returns>
        T Map<T>(object source);

        /// <summary>
        /// Converts the given source object to the specified destination object
        /// </summary>
        /// <param name="source">The source object</param>
        /// <param name="target">The destination type</param>
        /// <returns>The converted object</returns>
        object Map(object source, Type target);
    }
}
