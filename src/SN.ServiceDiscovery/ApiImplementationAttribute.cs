﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Specifies a class that implements a public API, allowing easy discovery of known APIs
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false, Inherited=true)]
    public class ApiImplementationAttribute : Attribute
    {
    }
}
