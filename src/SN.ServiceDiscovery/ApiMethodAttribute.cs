﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Specifies an api method that will be exposed publicly
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false, Inherited=true)]
    public class ApiMethodAttribute : Attribute
    {
        /// <summary>
        /// Specifies an api method that will be exposed publicly
        /// </summary>
        /// <param name="path">The URL path that the API method will be bound to</param>
        public ApiMethodAttribute(string path)
        {
            this.Path = path;
            this.ApiRequestType = WebApiRequestType.GET;
        }

        /// <summary>
        /// The path of the API method under the API's path
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// The name of the API method, overrides the code-defined method.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// If true, defines that the method may only be accessed by authenticated users
        /// </summary>
        public bool AuthenticatedOnly { get; set; }

        /// <summary>
        /// If specified, defines the privilege ID which is required to access this method
        /// </summary>
        public string RequiredPrivilege { get; set; }

        /// <summary>
        /// If specified, the result will be converted to this type using the <seealso cref="IServiceTypeMapper"/>.
        /// </summary>
        public Type OutputType { get; set; }

        /// <summary>
        /// The type of web request this method will listen on
        /// </summary>
        public WebApiRequestType ApiRequestType { get; set; }
    }
}
