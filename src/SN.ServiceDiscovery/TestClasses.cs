﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Defines an object that can whatsit a whosit.
    /// </summary>
    /// <typeparam name="T">The type of thing to whatsit</typeparam>
    public interface IWhatsit<T>
    {
        /// <summary>
        /// Do the whatsit to the whosit.
        /// </summary>
        /// <param name="whosit">The whoosit to whatsit.</param>
        T DoWhatsit(T whosit);

        /// <summary>
        /// This thing is a jerk
        /// </summary>
        /// <param name="whosit">The whoosit to whatsit</param>
        /// <param name="numTimes">The number of times to whatsit</param>
        /// <returns>An array of something</returns>
        T[] DoWhatsit(T whosit, int numTimes);

        /// <summary>
        /// This will make me hate life.
        /// </summary>
        /// <param name="dingus">Thinger</param>
        void DoWhatsit(int dingus);
    }

    /// <summary>
    /// Presented pretty much to make things confusing
    /// </summary>
    public interface IWhatsit : IWhatsit<string>
    {
    }

    /// <summary>
    /// Do the whatsit with a string whosit.
    /// </summary>
    public class TestClasses : IWhatsit
    {
        /// <summary>
        /// Sometimes you just gotta hate being tasked with something
        /// </summary>
        /// <param name="whosit">A string whosit to whatsit.</param>
        public string DoWhatsit(string whosit)
        {
            return string.Format("Whatsit??? {0}.", whosit);
        }

        /// <summary>
        /// Do the whatsit to the whosit.
        /// </summary>
        /// <param name="dingus">The whoosit to whatsit.</param>
        public string DoWhatsit(int dingus)
        {
            return string.Empty;
        }

        /// <summary>
        /// This thing is a jerk
        /// </summary>
        /// <param name="whosit">The whoosit to whatsit</param>
        /// <param name="numTimes">The number of times to whatsit</param>
        /// <returns>An array of something</returns>
        public string[] DoWhatsit(string whosit, int numTimes)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This will make me hate life.
        /// </summary>
        /// <param name="dingus">Thinger</param>
        void IWhatsit<string>.DoWhatsit(int dingus)
        {
            throw new NotImplementedException();
        }
    }
}
