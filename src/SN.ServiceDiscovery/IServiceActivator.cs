﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Provides an extension point to allow activation of services on-demand
    /// </summary>
    public interface IServiceActivator
    {
        /// <summary>
        /// Activate the specified type and return the instance
        /// </summary>
        /// <typeparam name="T">The type of service to activate</typeparam>
        /// <param name="activationParameters">The array of parameters to use during activation</param>
        /// <returns>The activated object</returns>
        T Activate<T>(params Object[] activationParameters);

        /// <summary>
        /// Activate the specified type and return the instance
        /// </summary>
        /// <param name="serviceType">The type of service to activate</param>
        /// <param name="activationParameters">The array of parameters to use during activation</param>
        /// <returns>The activated object</returns>
        object Activate(Type serviceType, params object[] activationParameters);
    }
}
