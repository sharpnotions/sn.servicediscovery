﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Identifies an API parameter that needs special handling
    /// </summary>
    /// <remarks>
    /// Use this parameter to change the Type that is bound against the current
    /// http request.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ApiParameterAttribute : Attribute
    {
        /// <summary>
        /// Provides special handling for a parameter identified by the given name
        /// </summary>
        /// <param name="name">The name of the parameter that needs special handling</param>
        public ApiParameterAttribute(string name)
        {
            Name = name;
        }

        /// <summary>
        /// The name of the parameter to modify
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Identifies the parameter as bound to an argument in the URL
        /// </summary>
        public bool IsUrlParameter { get; set; }

        /// <summary>
        /// Specify a different type that should be used for the API request and then mapped
        /// into the api method's parameters.
        /// </summary>
        /// <remarks>
        /// You are responsible for providing the Type mapping configuration needed by your
        /// mapping impelentation library. If there is no mapping library present or if the
        /// library is unable to map from the bound type to the parameter type
        /// </remarks>
        public Type ApiRequestType { get; set; }
    }
}
