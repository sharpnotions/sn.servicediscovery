﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Defines information about a particular method endpoint within a service, such as the URL path
    /// and parameter information for this method endpoint.
    /// </summary>
    public class ServiceEndpointMetadata
    {
        private static readonly char[] PATH_SEPARATORS = new char[] { '/' };

        /// <summary>
        /// Create a new <see cref="ServiceEndpointMetadata"/> from the given defining type and endpoint method data.
        /// </summary>
        /// <param name="definitionType">The type which defines the API method</param>
        /// <param name="endpointMethod">The endpoint <see cref="MethodInfo"/></param>
        public ServiceEndpointMetadata(Type definitionType, MethodInfo endpointMethod)
        {
            CodeName = endpointMethod.Name;
            MethodHandle = endpointMethod;

            var methodAttribute = endpointMethod.GetCustomAttribute<ApiMethodAttribute>();

            //read basic information out of the method attribute
            ReadMethodAttribute(methodAttribute);

            if (IsValid)
            {
                GatherMethodArguments(endpointMethod);
                //capture asynchronous method information from the method itself
                AsyncInfo = AsyncMethodInfo.FromMethodInfo(endpointMethod);

                //check for any output type mappings defined on the result types if it is not
                //specified directly on the API method
                if(MapOutputToType == null)
                {
                    //no mapping specified on the attribute, check the actual result type instead
                    Type resultType = MethodHandle.ReturnType;
                    if(AsyncInfo.IsAsync && AsyncInfo.HasReturnType)
                    {
                        //consider the asynchronous return type for asynchronous methods
                        resultType = AsyncInfo.AsyncReturnType;
                    }
                    //if there is a return type, check for the ApiOutputMapAttribute on that type
                    if(resultType != null)
                    {
                        var outputMap = resultType.GetCustomAttribute<ApiOutputMapAttribute>();
                        if(outputMap != null && outputMap.ResultType != null)
                        {
                            //this type must be mapped, even when not overriden at the API level.
                            MapOutputToType = outputMap.ResultType;
                        }
                    }
                }
            } else
            {
                AsyncInfo = new AsyncMethodInfo();
            }
        }

        private void ReadMethodAttribute(ApiMethodAttribute attribute)
        {
            MethodName = string.IsNullOrWhiteSpace(attribute.Name) ? CodeName : attribute.Name;
            MethodPath = string.Join("/", (attribute.Path ?? string.Empty).Split(PATH_SEPARATORS, StringSplitOptions.RemoveEmptyEntries));

            IsAuthenticationRequired = attribute.AuthenticatedOnly;
            RequiredPrivilege = attribute.RequiredPrivilege;

            //if a privilege is specified it overrides AuthenticatedOnly=false
            IsAuthenticationRequired |= !string.IsNullOrWhiteSpace(RequiredPrivilege);

            RequestType = attribute.ApiRequestType;

            MapOutputToType = attribute.OutputType;

            IsValid = true;
        }


        private void GatherMethodArguments(MethodInfo endpointMethod)
        {
            //discover any parameter metadata 
            var apiParameters = endpointMethod
                .GetCustomAttributes<ApiParameterAttribute>()
                .ToDictionary(up => up.Name);

            Parameters = endpointMethod.GetParameters()
                .Select(mp => CreateParameterMetadata(mp, apiParameters))
                .ToArray();
        }

        private ServiceParameterMetadata CreateParameterMetadata(ParameterInfo paramInfo, Dictionary<string, ApiParameterAttribute> apiParameters)
        {
            var paramHasAttribute = apiParameters.ContainsKey(paramInfo.Name);
            if (paramHasAttribute)
            {
                var serviceAttribute = apiParameters[paramInfo.Name];
                return new ServiceParameterMetadata(paramInfo, serviceAttribute);
            }
            return new ServiceParameterMetadata(paramInfo);
        }

        /// <summary>
        /// Returns true if the endpoint is valid
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// If not <seealso cref="IsValid"/>, contains a textual reason why the endpoint isn't valid
        /// </summary>
        public string ReasonInvalid { get; private set; }

        /// <summary>
        /// The name of the api method as defined in code
        /// </summary>
        public string CodeName { get; private set; }

        /// <summary>
        /// The <see cref="MethodInfo"/> for the API method
        /// </summary>
        public MethodInfo MethodHandle { get; private set; }

        /// <summary>
        /// The friendly name of the api method
        /// </summary>
        public string MethodName { get; private set; }

        /// <summary>
        /// The path of the method underneath this api's path
        /// </summary>
        public string MethodPath { get; private set; }

        /// <summary>
        /// Returns true if the user must be authenticated
        /// </summary>
        public bool IsAuthenticationRequired { get; private set; }

        /// <summary>
        /// Returns <c>true</c> if the task is asynchronous
        /// </summary>
        public AsyncMethodInfo AsyncInfo { get; private set; }

        /// <summary>
        /// If not null, specifies the privilege ID required to access this method
        /// </summary>
        public string RequiredPrivilege { get; private set; }

        /// <summary>
        /// The type of http request this method will respond to
        /// </summary>
        public WebApiRequestType RequestType { get; private set; }

        /// <summary>
        /// If specified, the type that the API will return to the calling user.
        /// </summary>
        public Type MapOutputToType { get; private set; }

        /// <summary>
        /// The collection of parameter information collected during discovery.
        /// </summary>
        public IEnumerable<ServiceParameterMetadata> Parameters { get; private set; }
    }

    /// <summary>
    /// Information about an asynchronous endpint method
    /// </summary>
    public class AsyncMethodInfo
    {
        private static readonly Type TASK_TYPE = typeof(Task);

        /// <summary>
        /// Create a new AsyncMethodInfo from the specified MethodInfo
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static AsyncMethodInfo FromMethodInfo(MethodInfo method)
        {
            var info = new AsyncMethodInfo();
            //determine if the method's return type is a Task of some kind
            info.IsAsync = TASK_TYPE.IsAssignableFrom(method.ReturnType);
            if (info.IsAsync)
            {
                //determine if the asynchronous method has a return type or not
                info.HasReturnType = method.ReturnType.IsConstructedGenericType;
                if (info.HasReturnType)
                {
                    //it has a reurn type, so deconstruct the generic task to get the resulting type
                    info.AsyncReturnType = method.ReturnType.GetGenericArguments().FirstOrDefault();
                }
            }
            return info;
        }

        /// <summary>
        /// Indicates whether this method is asynchronous
        /// </summary>
        public bool IsAsync { get; private set; }

        /// <summary>
        /// Indicates if this asynchronous method has a return type
        /// </summary>
        public bool HasReturnType { get; private set; }

        /// <summary>
        /// Indicates the type of item returned from the asynchronous method
        /// </summary>
        public Type AsyncReturnType { get; private set; }
    }
}
