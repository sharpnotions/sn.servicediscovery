﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// Defines an interface for plugins that will determine the activation
    /// parameters to supply to the <seealso cref="IServiceActivator"/> implementation.
    /// </summary>
    public interface IServiceActivatorParameterProvider
    {
        /// <summary>
        /// Inspect the generator's runtime environment to return a needed activation parameter
        /// </summary>
        /// <param name="environment">The executing generator's current runtime environment</param>
        /// <returns>An object that can be used as a parameter to the <seealso cref="IServiceActivator"/>'s Activate method</returns>
        object GetParameter(object environment);
    }
}
