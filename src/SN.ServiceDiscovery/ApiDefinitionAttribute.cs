﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery
{
    /// <summary>
    /// This attribute indicates that the specified interface should be discovered as a public API and exposed accordingly.
    /// </summary>
    /// <remarks>
    /// This attribute allows the service discovery mechanism to determine which implemented interfaces have 
    /// </remarks>
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple=false, Inherited=false)]
    public class ApiDefinitionAttribute : AbstractApiDefinitionAttribute
    {
        /// <summary>
        /// Creates a new <see cref="ApiDefinitionAttribute"/> with the specified path.
        /// </summary>
        /// <param name="path">The base path of the API endpoints under the API root path</param>
        public ApiDefinitionAttribute(string path)
        {
            this.Path = path;
        }

        /// <summary>
        /// The path of the API underneath the API root of the server
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// If the api requires authenticated access.
        /// </summary>
        public bool AuthenticatedOnly { get; set; }

        /// <summary>
        /// Specifies the required privilege ID necessary to access this API. If specified, AuthenticatedOnly is set to true
        /// </summary>
        public string RequiredPrivilege { get; set; }
    }
}