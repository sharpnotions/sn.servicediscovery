﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.NancyFx
{
    /// <summary>
    /// A metadata class to communicate information about a service endpoint to the <see cref="ApiModule"/>
    /// </summary>
    internal class SimpleServiceMetadata
    {
        /// <summary>
        /// The URL pattern that the service endpoint needs to respond on
        /// </summary>
        public string EndpointUrl { get; internal set; }

        /// <summary>
        /// The type of HTTP request that the endpoint URL should be bound to
        /// </summary>
        public WebApiRequestType RequestType { get; internal set; }

        /// <summary>
        /// <c>true</c> if user authentication is required
        /// </summary>
        public bool AuthenticationRequired { get; set; }

        /// <summary>
        /// <c>true</c> is the user must have a specific authorized role
        /// </summary>
        public bool AuthorizationRequired { get; set; }

        /// <summary>
        /// The list of roles that the current user must have
        /// </summary>
        public string[] AuthorizedRoles { get; set; }

        /// <summary>
        /// Non-null if the output of the method must be mapped to a different model for output
        /// </summary>
        public Type MapOutputToType { get; set; }

        /// <summary>
        /// The type of the class to actually activate during execution
        /// </summary>
        public Type ServiceType { get; set; }

        /// <summary>
        /// The method to execute on the activated service. See also: <seealso cref="ServiceType"/>
        /// </summary>
        public MethodInfo EndpointMethod { get; internal set; }

        /// <summary>
        /// The list of parameter information describing the <seealso cref="EndpointMethod"/>'s parameters.
        /// </summary>
        public List<ServiceParameterMetadata> MethodParameters { get; set; }

        /// <summary>
        /// Information describing the asynchronous execution of the <seealso cref="EndpointMethod"/>, if any.
        /// </summary>
        public AsyncMethodInfo AsyncInfo { get; internal set; }

        public override string ToString()
        {
            return string.Format("{0}: {1} {2}", GetType().Name, RequestType, EndpointUrl);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is SimpleServiceMetadata)) return false;
            var ssm = (SimpleServiceMetadata)obj;
            return ssm.RequestType == this.RequestType && ssm.EndpointUrl.Equals(this.EndpointUrl, StringComparison.Ordinal);
        }

        public override int GetHashCode()
        {
            return RequestType.GetHashCode() ^ EndpointUrl.GetHashCode();
        }
    }
}
