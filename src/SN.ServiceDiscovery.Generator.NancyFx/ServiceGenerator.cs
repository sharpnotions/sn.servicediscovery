﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.NancyFx
{
    /// <summary>
    /// Consumes a <see cref="DiscoveryCore"/> to produce API classes for Nancy.
    /// See: http://nancyfx.org
    /// </summary>
    public class ServiceGenerator
    {
        /// <summary>
        /// Creates a new <see cref="ServiceGenerator"/>, registering activators, type
        /// mappers, and service endpoints in the <see cref="ApiModule"/>.
        /// </summary>
        /// <param name="core">The <see cref="DiscoveryCore"/> describing the discovered services in this application.</param>
        public ServiceGenerator(DiscoveryCore core)
        {
            //move to static variables to help get rid of local closure variables
            ApiModule._activator = core.Config.ServiceActivator;
            ApiModule._mapper = core.Config.TypeMapper;

            foreach (var service in core.Services.Where(svc=>svc.IsValid))
            {
                CreateService(service);
            }
        }

        /// <summary>
        /// Maps the endpoints in a service out to metadata and registers them on the <see cref="ApiModule"/>.
        /// </summary>
        /// <param name="service">The service to process</param>
        private void CreateService(ServiceMetadata service)
        {
            foreach (var endpoint in service.Endpoints.Where(ep => ep.IsValid))
            {
                //create the api method metadata
                var meta = CreateMetadata(service, endpoint);
                ApiModule.AddApiMethod(meta);
            }
        }

        /// <summary>
        /// Creates a simple set of service metadata needed to execute the API method from a web API
        /// </summary>
        /// <param name="serviceInfo">The <see cref="ServiceMetadata"/> information.</param>
        /// <param name="endpointInfo"></param>
        /// <returns></returns>
        private static SimpleServiceMetadata CreateMetadata(ServiceMetadata serviceInfo, ServiceEndpointMetadata endpointInfo)
        {
            var simpleMetadata = new SimpleServiceMetadata();
            simpleMetadata.ServiceType = serviceInfo.ImplementationType;
            simpleMetadata.EndpointMethod = endpointInfo.MethodHandle;
            simpleMetadata.MapOutputToType = endpointInfo.MapOutputToType;
            simpleMetadata.MethodParameters = endpointInfo.Parameters.ToList();
            simpleMetadata.AsyncInfo = endpointInfo.AsyncInfo;

            AddSecurity(simpleMetadata, serviceInfo, endpointInfo);

            //format the URL for the endpoint
            string handlerUrl;
            if (string.IsNullOrEmpty(endpointInfo.MethodPath))
            {
                handlerUrl = serviceInfo.ServicePath;
            }
            else
            {
                handlerUrl = string.Format("{0}/{1}", serviceInfo.ServicePath, endpointInfo.MethodPath);
            }
            simpleMetadata.EndpointUrl = handlerUrl;
            simpleMetadata.RequestType = endpointInfo.RequestType;
            return simpleMetadata;
        }

        /// <summary>
        /// Adds security metadata to the simpleMetadata from the Service and Endpoint metadata objects
        /// </summary>
        /// <param name="simpleMetadata">The <see cref="SimpleServiceMetadata"/> object to modify.</param>
        /// <param name="serviceInfo">The <see cref="ServiceMetadata"/> information.</param>
        /// <param name="endpointInfo">The <see cref="ServiceEndpointMetadata"/> information.</param>
        private static void AddSecurity(SimpleServiceMetadata simpleMetadata, ServiceMetadata serviceInfo, ServiceEndpointMetadata endpointInfo)
        {
            if (serviceInfo.IsAuthenticationRequired || endpointInfo.IsAuthenticationRequired || (serviceInfo.RequiredPrivilege ?? endpointInfo.RequiredPrivilege) != null)
            {
                simpleMetadata.AuthenticationRequired = true;
                simpleMetadata.AuthorizedRoles = (new[] { serviceInfo.RequiredPrivilege, endpointInfo.RequiredPrivilege }).Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();
                simpleMetadata.AuthorizationRequired = simpleMetadata.AuthorizedRoles.Any();
            }
        }
    }
}
