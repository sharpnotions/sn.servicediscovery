﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.NancyFx
{
    public class ApiResponse
    {
        public object Data { get; private set; }

        public ApiResponse(object value)
        {
            Data = value;
        }
    }
}
