﻿using Nancy;
using Nancy.Responses.Negotiation;
using Nancy.ViewEngines.SuperSimpleViewEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.NancyFx
{
    public class JsonDumpViewExtension : ISuperSimpleViewEngineMatcher
    {
        private static readonly Regex JsonDumpPattern = new Regex(@"@JsonDump", RegexOptions.Compiled);

        private ISerializer _json = null;

        public JsonDumpViewExtension(IEnumerable<ISerializer> serializers)
        {
            _json = serializers.FirstOrDefault(s => s.CanSerialize("application/json"));
        }

        public string Invoke(string content, dynamic model, IViewEngineHost host)
        {
            return JsonDumpPattern.Replace(
                content,
                m =>
                {
                    var result = "No data";
                    try
                    {
                        using (var stream = new System.IO.MemoryStream())
                        {
                            _json.Serialize("application/json", model, stream);
                            stream.Position = 0;
                            using (var reader = new System.IO.StreamReader(stream))
                            {
                                result = reader.ReadToEnd();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = ex.Message;
                    }
                    
                    return result;
                });
        }
    }
}
