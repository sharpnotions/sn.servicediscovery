﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SN.HybridViewLocator;
using Nancy.Security;
using Nancy.Validation;
using System.Threading;
using SN.ServiceDiscovery.Exceptions;
using System.Collections;

//This assembly attribute allows the custom view locator to find views
//based upon Model names
[assembly: EmbeddedViewLocations("SN.ServiceDiscovery.Generator.NancyFx")]

namespace SN.ServiceDiscovery.Generator.NancyFx
{
    /// <summary>
    /// An API module - exposes various data APIs under the /api/ suffix
    /// </summary>
    public class ApiModule : NancyModule
    {
        /// <summary>
        /// The instance of the <see cref="IServiceActivator"/> service to use to activate services.
        /// </summary>
        internal static IServiceActivator _activator = null;

        /// <summary>
        /// The instance of the <see cref="IServiceTypeMapper"/> service to use when mapping inputs and output.
        /// </summary>
        internal static IServiceTypeMapper _mapper = null;

        /// <summary>
        /// The list of service endpoints that this API controller is exposing.
        /// </summary>
        private static List<SimpleServiceMetadata> _services = new List<SimpleServiceMetadata>();

        /// <summary>
        /// The list of <see cref="IServiceActivatorParameterProvider"/> implementations that will be executed
        /// to provide parameters to the <see cref="_activator"/>.
        /// </summary>
        private IEnumerable<IServiceActivatorParameterProvider> _parameterProviders = null;

        /// <summary>
        /// Create a new <see cref="ApiModule"/>.
        /// </summary>
        /// <param name="parameterProviders">The parameter providers to use when handling this request.</param>
        public ApiModule(IEnumerable<IServiceActivatorParameterProvider> parameterProviders) : base("api")
        {
            Before.AddItemToEndOfPipeline(ctx =>
            {
                //indicate that this module can utilize an embedded view for object results
                this.UseEmbeddedViews();
                return null;
            });


            //store the parameter providers for later
            _parameterProviders = parameterProviders ?? new IServiceActivatorParameterProvider[0];

            foreach (var item in _services)
            {
                //grab the correct type of HTTP method router to bind this endpoint to
                RouteBuilder toBind = null;
                switch (item.RequestType)
                {
                    case WebApiRequestType.GET:
                        toBind = Get;
                        break;
                    case WebApiRequestType.POST:
                        toBind = Post;
                        break;
                    case WebApiRequestType.PUT:
                        toBind = Put;
                        break;
                    case WebApiRequestType.DELETE:
                        toBind = Delete;
                        break;
                }

                //bind the method to the appropriate sync/async handler
                if (item.AsyncInfo.IsAsync)
                {
                    toBind[item.EndpointUrl, true] = AsyncHandler;
                } else
                {
                    toBind[item.EndpointUrl] = SynchronousHandler;
                }
            }

            //a debugg list of known services
            Get["/"] = args =>
            {
                return new ApiResponse(_services
                    .Select(s => string.Format("{0} {1}", s.RequestType, s.EndpointUrl))
                    .ToArray());
            };
            Get["debug"] = args => new ApiResponse(_services);
        }

        /// <summary>
        /// Retrieves the correct handler for this type of request
        /// </summary>
        /// <returns>A generated API method</returns>
        private SimpleServiceMetadata GetServiceMetadata()
        {
            //retrieve the HTTP method used to access the API module
            var methodText = Context.ResolvedRoute.Description.Method.ToString();

            //trim the incoming route's path to get rid of the module path and leading slashes
            var requestPath = TrimModulePath(ModulePath, Context.ResolvedRoute.Description.Path);

            //attempt to find a handler by comparing the request type and route path against the handlers list
            var handler = _services
                .SingleOrDefault(h => h.RequestType.ToString() == methodText
                    && h.EndpointUrl.Equals(requestPath, StringComparison.OrdinalIgnoreCase));

            //ensure a handler was found
            if (handler == null) throw new Exception("No handler for the matched path could be found.");

            return handler;
        }

        /// <summary>
        /// Performs some informed guessing about the structure of the request path and attempts
        /// to figure out the actual path to the API module being requested.
        /// </summary>
        /// <param name="modulePath">The URL path of the Nancy module performing the processing</param>
        /// <param name="resolvedPath">The path that the URL resolution context finally settled on</param>
        /// <returns>A string with the cleaned path</returns>
        private static string TrimModulePath(string modulePath, string resolvedPath)
        {
            var slashes = new[] { '/' };

            if (modulePath == null) throw new ArgumentNullException("modulePath");
            if (resolvedPath == null) throw new ArgumentNullException("resolvedPath");

            string newPath = resolvedPath;
            do
            {
                resolvedPath = newPath;
                newPath = resolvedPath.TrimStart(slashes);
                if (newPath.StartsWith(modulePath))
                {
                    newPath = string.Concat(newPath.Skip(modulePath.Length));
                }

            } while (resolvedPath != newPath);

            return newPath;
        }

        /// <summary>
        /// Add a handler to the API module. This is utilized by the <see cref="ServiceGenerator"/>
        /// </summary>
        /// <param name="handler">The handler to add</param>
        internal static void AddApiMethod(SimpleServiceMetadata handler)
        {
            if (!_services.Contains(handler))
            {
                _services.Add(handler);
            }
        }

        /// <summary>
        /// Handle a request backed by a synchronous API call
        /// </summary>
        /// <param name="url">The url parameters parsed out by Nancy</param>
        /// <returns>The result of the API call, or various HTTP status codes based on API execution</returns>
        private dynamic SynchronousHandler(dynamic url)
        {
            ServiceSetupResult setupResult = SetupApi(url);
            if (setupResult.EndRequestEarly)
            {
                return setupResult.EarlyExitResponseCode;
            }

            //invoke the service
            try
            {
                object apiResult = setupResult.Metadata.EndpointMethod.Invoke(setupResult.Service, setupResult.Parameters.ToArray());
                return HandleApiResult(apiResult, setupResult.Metadata);
            } catch(Exception ex)
            {
                return HandleException(ex.InnerException);
            }

        }

        /// <summary>
        /// Handle a request backed by an asynchronous API call
        /// </summary>
        /// <param name="url">The url parameters parsed out by nancy</param>
        /// <param name="ct">The cancellation token for this request</param>
        /// <returns>The result of the API call, or various HTTP status codes based on API execution</returns>
        private async Task<dynamic> AsyncHandler(dynamic url, CancellationToken ct)
        {
            ServiceSetupResult setupResult = SetupApi(url);
            if(setupResult.EndRequestEarly)
            {
                return setupResult.EarlyExitResponseCode;
            }

            return await Task.Run<dynamic>(() =>
            {
                try
                {
                    var endpointTask = setupResult.Metadata.EndpointMethod.Invoke(setupResult.Service, setupResult.Parameters.ToArray());
                    //ensure that the invoke returned some kind of task
                    if(endpointTask != null)
                    {
                        if(endpointTask is Task)
                        {
                            //cast the task and wait for it, using the cancellation token from the external server
                            var t = (Task)endpointTask;
                            t.Wait(ct);
                            //check to see if there was some kind of return type
                            if (setupResult.Metadata.AsyncInfo.HasReturnType)
                            {
                                //Reflect the Task<>.Result property in order to retrieve the result of the API action
                                var resultProperty = typeof(Task<>)
                                    .MakeGenericType(setupResult.Metadata.AsyncInfo.AsyncReturnType)
                                    .GetProperty("Result");
                                //retrieve the result of the Task execution
                                object result = resultProperty.GetValue(t);
                                //then handle the response using the unified execution path
                                return HandleApiResult(result, setupResult.Metadata);
                            } else
                            {
                                //no return type, just spit out "accepted"
                                return HttpStatusCode.Accepted;
                            }
                        } else
                        {
                            return HandleApiResult(endpointTask, setupResult.Metadata);
                        }
                    }
                    //this should have been a task, return an error
                    return HttpStatusCode.InternalServerError;
                } catch(AggregateException aex)
                {
                    //flatten an aggregate exception before passing the inner exception to the
                    //unified exception handler. This retrieves the actual exception thrown out of
                    //the API method, rather than something task-related.
                    return HandleException(aex.Flatten().InnerExceptions.FirstOrDefault());
                } catch(Exception ex)
                {
                    return HandleException(ex);
                }
            });
        }

        /// <summary>
        /// Performs the common startup procedures for the requested API.
        /// </summary>
        /// <remarks>
        /// Retrieves the handler, checks for authentication/authorization, binds/validates/converts
        /// parameters, and activates the service object with the configured service activator.
        /// </remarks>
        /// <param name="url">The url parameters as parsed by Nancy</param>
        /// <returns>A <see cref="ServiceSetupResult"/>.</returns>
        private ServiceSetupResult SetupApi(dynamic url)
        {
            var simple = GetServiceMetadata();
            DynamicDictionary urlDictionary = (DynamicDictionary)url;

            //check to see if any authentication is required
            if (simple.AuthenticationRequired)
            {
                //ensure there is a user
                if (Context.CurrentUser == null)
                {
                    return new ServiceSetupResult(HttpStatusCode.Unauthorized);
                }

                //check to see if roles have been specified
                if (simple.AuthorizationRequired)
                {
                    //ensure the required roles are on the current user
                    if (!Context.CurrentUser.HasClaims(simple.AuthorizedRoles))
                    {
                        return new ServiceSetupResult(HttpStatusCode.Forbidden);
                    }
                }
            }

            List<object> methodParameters = new List<object>();
            List<ModelValidationError> methodParameterErrors = new List<ModelValidationError>();

            //process each bit of parameter metadata into a method parameter bound from the request
            foreach (var parameterInfo in simple.MethodParameters)
            {
                if (parameterInfo.IsUrlBound)
                {
                    //url binding
                    var urlParam = (DynamicDictionaryValue)urlDictionary[parameterInfo.ParameterName];
                    if (urlParam.HasValue) {
                        methodParameters.Add(Convert.ChangeType(urlParam.Value, parameterInfo.ParameterType));
                    }
                    else
                    {
                        //optional parameters can have their default inserted in place of null
                        if (parameterInfo.IsOptional) {
                            methodParameters.Add(parameterInfo.OptionalParameterDefaultValue);
                        }
                        else {
                            //otherwise, add in a required error
                            methodParameterErrors.Add(new ModelValidationError(parameterInfo.ParameterName, "required"));
                        }
                    }
                }
                else
                {
                    //body binding
                    var bindingGenericMethod = typeof(Nancy.ModelBinding.ModuleExtensions).GetMethod("Bind", new Type[] { typeof(NancyModule) });
                    //convert the untyped generic into a typed generic method
                    var concreteMethod = bindingGenericMethod.MakeGenericMethod(new[] { parameterInfo.ApiRequestType ?? parameterInfo.ParameterType });
                    //method is static, call will null owner, provide module as argument object
                    object boundModel = concreteMethod.Invoke(null, new object[] { this });

                    //call the custom validation method to ensure that it validates with the incoming model's
                    //validation rules, and not the validation rules for [object]
                    var validation = ValidateGeneric(boundModel);
                    if (!validation.IsValid)
                    {
                        methodParameterErrors.AddRange(
                            validation.Errors.Values.SelectMany(e => e)
                        );
                    }
                    //check to see if we need to perform a mapping from the currently
                    //bound request-model to the actual method parameter type
                    if (parameterInfo.ApiRequestType != null)
                    {
                        boundModel = _mapper.Map(boundModel, parameterInfo.ParameterType);
                    }
                    methodParameters.Add(boundModel);
                }
            }

            //check for any validation errors
            if (methodParameterErrors.Any())
            {
                Context.ModelValidationResult = new ModelValidationResult(methodParameterErrors);
                return new ServiceSetupResult(HttpStatusCode.BadRequest);
            }


            //query any known activation parameter providers for their parameters
            //given this module/request environment
            var serviceActivationParameters = _parameterProviders
                .Select(provider => provider.GetParameter(this))
                .ToArray();

            //activate the service
            var service = _activator.Activate(simple.ServiceType, serviceActivationParameters);

            return new ServiceSetupResult(service, methodParameters, simple);
        }

        /// <summary>
        /// Performs typed validation on a <seealso cref="object"/> instance by reflecting on the
        /// <see cref="Nancy.Validation.ModuleExtensions.Validate{T}(INancyModule, T)"/> method.
        /// </summary>
        /// <param name="obj">The object to validate.</param>
        /// <returns>A <see cref="ModelValidationResult"/>.</returns>
        private ModelValidationResult ValidateGeneric(object obj)
        {
            var validateMethod = typeof(Nancy.Validation.ModuleExtensions).GetMethod("Validate");
            var genericMethod = validateMethod.MakeGenericMethod(new[] { obj.GetType() });
            var result = genericMethod.Invoke(null, new object[] { this, obj });
            return (ModelValidationResult)result;
        }

        /// <summary>
        /// Performs inspection of the result of any given API call and responds appropriately.
        /// </summary>
        /// <param name="result">The result of calling the api method</param>
        /// <param name="meta">The service metadata for the API call</param>
        /// <returns>A response of some kind.</returns>
        private dynamic HandleApiResult(object result, SimpleServiceMetadata meta)
        {
            if (result == null)
            {
                if(meta.EndpointMethod.ReturnType == typeof(void))
                {
                    return HttpStatusCode.Accepted;
                } else
                {
                    return HttpStatusCode.NotFound;
                }
            }
            //status codes just kick out right away
            if (result is HttpStatusCode)
                return result;

            //determine if the type needs to be mapped
            result = ResultMapping(result, meta);
            return new ApiResponse(result);
        }

        /// <summary>
        /// Performs any result mapping that may be needed
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="meta">The metadata describing this request's configuration.</param>
        /// <returns>A mapped object that may be returned as an API result</returns>
        private object ResultMapping(object result, SimpleServiceMetadata meta)
        {
            //simplest mapping scenario is when the output type is already known via metadata
            if (meta.MapOutputToType != null)
            {
                //map output was specified in the method metadata, so just map
                return _mapper.Map(result, meta.MapOutputToType);
            }

            //try to determine if the result type must be mapped in some way
            if(result != null)
            {
                var resultType = result.GetType();
                var outputMapAttribute = resultType.GetCustomAttributes(false).OfType<ApiOutputMapAttribute>().FirstOrDefault();
                if(outputMapAttribute != null)
                {
                    //output map attribute was present directly on the output type, so map it!
                    result = _mapper.Map(result, outputMapAttribute.ResultType);
                } else if (typeof(IEnumerable).IsAssignableFrom(resultType) && resultType.IsConstructedGenericType)
                {
                    //get the original generic type arguments to the collection
                    var genericTypes = resultType.GetGenericArguments();
                    //determine if any of these types are mapped
                    var mappedTypeAttributes = genericTypes.Select(t => t.GetCustomAttributes(false).OfType<ApiOutputMapAttribute>().FirstOrDefault()).ToArray();
                    if (mappedTypeAttributes.Any(t => t != null))
                    {
                        //at least one type was mapped, so get the generic definition for the collection type
                        var genericCollection = resultType.GetGenericTypeDefinition();
                        //create an array where we substitute in the mapped types for current types, where necessary
                        var mappedTypes = mappedTypeAttributes.Select((mt, idx) => mt == null ? genericTypes[idx] : mt.ResultType).ToArray();
                        //create the new constructed generic collection type
                        var constructedGenericCollection = genericCollection.MakeGenericType(mappedTypes);
                        //...and feed it to the mapper
                        result = _mapper.Map(result, constructedGenericCollection);
                    }
                } else if (resultType.IsArray)
                {
                    //get the type for the array elements
                    var arrayOutputType = resultType.GetElementType();
                    //determine if the array element type is mapped
                    var arrayOutputMapAttribute = arrayOutputType.GetCustomAttributes(false).OfType<ApiOutputMapAttribute>().FirstOrDefault();
                    if(arrayOutputMapAttribute != null)
                    {
                        //make a new array type for the mapped result type with the same array rank as the original result
                        var mappedArrayType = arrayOutputMapAttribute.ResultType.MakeArrayType(arrayOutputType.GetArrayRank());
                        //...and feed it to the mapper
                        result = _mapper.Map(result, mappedArrayType);
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// Performs inspection of exceptions from the API invocation
        /// </summary>
        /// <param name="ex">The exception that was thrown</param>
        /// <returns>The proper http status code for the encountered exception</returns>
        private dynamic HandleException(Exception ex)
        {
            if(ex!= null)
            {
                //look for errors exposed by the service discovery library
                if(ex is ItemNotFoundException)
                {
                    return HttpStatusCode.NotFound;
                }
                if(ex is UserNotAuthenticatedException)
                {
                    return HttpStatusCode.Unauthorized;
                }
                if(ex is UserNotAuthenticatedException)
                {
                    return HttpStatusCode.Forbidden;
                }
                if(ex is ValidationException)
                {
                    return HttpStatusCode.BadRequest;
                }
            }

            Context.Trace.TraceLog.WriteLog(sb =>
            {
                sb.AppendFormat("Unknown exception encountered while processing generic API: {0}", ex);
            });
            return HttpStatusCode.InternalServerError;
        }

        /// <summary>
        /// The results of setting up a service
        /// </summary>
        private class ServiceSetupResult
        {
            /// <summary>
            /// Create a new <see cref="ServiceSetupResult"/>, with the specified early exit request code.
            /// </summary>
            /// <param name="earlyExitResponseCode"></param>
            public ServiceSetupResult(HttpStatusCode earlyExitResponseCode)
            {
                EndRequestEarly = true;
                EarlyExitResponseCode = earlyExitResponseCode;
            }

            /// <summary>
            /// Create a new <see cref="ServiceSetupResult"/>, with the specified service, parameters, and metadata.
            /// </summary>
            /// <param name="service">The activated service</param>
            /// <param name="parameters">The endpoint method parameters</param>
            /// <param name="metadata">The metadata describing the endpoint</param>
            public ServiceSetupResult(object service, List<object> parameters, SimpleServiceMetadata metadata)
            {
                Service = service;
                Parameters = parameters;
                Metadata = metadata;
            }
            
            /// <summary>
            /// If the request should end before the API is invoked
            /// </summary>
            public bool EndRequestEarly { get; set; }

            /// <summary>
            /// The response code that should be used for an early exit
            /// </summary>
            public HttpStatusCode EarlyExitResponseCode { get; set; }

            /// <summary>
            /// The activated service containing the API method
            /// </summary>
            public object Service { get; set; }

            /// <summary>
            /// The bound parameters to the API method
            /// </summary>
            public List<object> Parameters { get; set; }

            /// <summary>
            /// The metadata for the API method
            /// </summary>
            public SimpleServiceMetadata Metadata { get; set; }
        }
    }
}
