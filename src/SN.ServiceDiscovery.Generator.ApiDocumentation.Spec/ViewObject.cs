﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Spec
{
    /// <summary>
    /// Creates a viewmodel for the specified type by pulling in the objects values, and converting
    /// the Id property to a ulong when possible.
    /// </summary>
    /// <typeparam name="T">The type of model to generate a view for.</typeparam>
    public class ViewObject<T> : DynamicObject where T : StoredObjectBase
    {
        private static readonly Regex _idTester = new Regex(@"^[^/]+/(\d+)$", RegexOptions.Compiled);
        private Dictionary<string, object> _fields = new Dictionary<string, object>();

        /// <summary>
        /// The list of properties that won't be automatically transferred to the viewmodel dictionary
        /// </summary>
        protected IList<string> _ignoredFields = new List<string>() { "Id" };

        /// <summary>
        /// Creates a new <see cref="ViewObject"/> and harvests all of the required values
        /// </summary>
        /// <param name="dataObject"></param>
        public ViewObject(T dataObject)
        {
            GatherValues(dataObject);
        }

        /// <summary>
        /// Shorthand to trigger gathering fields, properties, and the Id.
        /// </summary>
        /// <param name="dataObject"></param>
        protected void GatherValues(T dataObject)
        {
            GatherFields(dataObject);
            GatherProperties(dataObject);
            GatherId(dataObject);
        }

        /// <summary>
        /// Gathers the field values from the object and deposits them in the dynamic dictionary
        /// </summary>
        /// <param name="dataObject"></param>
        protected void GatherFields(T dataObject)
        {
            var fullType = dataObject.GetType();
            fullType.GetFields(BindingFlags.Instance | BindingFlags.Public)
                .Where(f => !_ignoredFields.Contains(f.Name))
                .ToList()
                .ForEach(f => { _fields[f.Name] = f.GetValue(dataObject); });
        }

        /// <summary>
        /// Gathers property values from the object and deposits them in the dynamic dictionary
        /// </summary>
        /// <param name="dataObject"></param>
        protected void GatherProperties(T dataObject)
        {
            var fullType = dataObject.GetType();
            fullType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => !_ignoredFields.Contains(p.Name))
                .ToList()
                .ForEach(p => { _fields[p.Name] = p.GetValue(dataObject); });
        }

        /// <summary>
        /// Gathers & modifies the ID field value from the object and deposits it in the dynamic dictionary
        /// </summary>
        /// <param name="dataObject"></param>
        protected void GatherId(T dataObject)
        {
            var idMatch = _idTester.Match(dataObject.Id ?? string.Empty);
            if (idMatch.Success)
                _fields["Id"] = ulong.Parse(idMatch.Groups[1].Value);
            else
                _fields["Id"] = dataObject.Id;
        }

        /// <summary>
        /// Retrieves the dynamic members on this dynamic object
        /// </summary>
        /// <returns>The list of properties available on this dynamic object</returns>
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _fields.Keys.AsEnumerable();
        }

        /// <summary>
        /// Tries to get the specified member
        /// </summary>
        /// <param name="binder">The binder specifying the member to get</param>
        /// <param name="result">The result object to populate</param>
        /// <returns>True if the value is present in this dynamic object</returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return _fields.TryGetValue(binder.Name, out result);
        }

        /// <summary>
        /// Tries to set the specified member
        /// </summary>
        /// <param name="binder">The binder specifying the member to set</param>
        /// <param name="value">The value to set on this object</param>
        /// <returns>false, because view models are readonly</returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            return false;
        }
    }

    public abstract class StoredObjectBase
    {
        protected StoredObjectBase()
        {
            Active = true;
        }

        public string Id { get; set; }

        public DateTimeOffset Modified { get; set; }

        public bool Active { get; set; }
    }

    public abstract class LocalizedObjectBase : StoredObjectBase
    {
        public string FriendlyName { get; set; }

        public string PhraseId { get; set; }
    }

    public class TestClass : LocalizedObjectBase
    {

    }

    
    public class ViewObjectTests
    {
        [Fact(DisplayName="Retrieves all properties")]
        public void RetrievesAllProperties()
        {
            var vo = new ViewObject<TestClass>(new TestClass { Id = "test/123" });
            var jobject = Newtonsoft.Json.Linq.JObject.FromObject(vo);
            jobject.GetValue("FriendlyName");
            jobject.GetValue("PhraseId");
            jobject.GetValue("Active");
            jobject.GetValue("Modified");
            var token = jobject.GetValue("Id");

            Assert.Equal((ulong)123, (ulong)token);
        }
    }
}
