﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Spec
{
    public class ExampleModelTests
    {
        [Fact(DisplayName = "Model_for_simple_class")]
        public void Model_for_simple_class()
        {
            var simpleModel = Utility.GetExampleModel(typeof(SampleClass1));
            Assert.NotNull(simpleModel);
            Assert.NotEqual(string.Empty, simpleModel);
        }

        [Fact(DisplayName = "Model_for_list")]
        public void Model_for_list()
        {
            var listModel = Utility.GetExampleModel(typeof(List<SampleClass1>));
            Assert.NotNull(listModel);
            Assert.NotEqual(string.Empty, listModel);
        }

        [Fact(DisplayName = "Model_for_dictionary")]
        public void Model_for_dictionary()
        {
            var dictionaryModel = Utility.GetExampleModel(typeof(Dictionary<string, SampleClass1>));
            Assert.NotNull(dictionaryModel);
            Assert.NotEqual(string.Empty, dictionaryModel);
        }

        [Fact(DisplayName = "Model_for_self_recursive")]
        public void Model_for_self_recursive()
        {
            var recursiveModel = Utility.GetExampleModel(typeof(SampleSelfReferential));
            Assert.NotNull(recursiveModel);
            Assert.NotEqual(string.Empty, recursiveModel);
        }

        public class SampleClass1
        {
            public string Something { get; set; }

            public int SomethingElse { get; set; }
        }

        public class SampleSelfReferential
        {
            public string Name { get; set; }

            public int NumberOfRecusions;

            public List<SampleSelfReferential> Children { get; set; }
        }
    }
}
