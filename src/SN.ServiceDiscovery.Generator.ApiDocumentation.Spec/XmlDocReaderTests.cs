﻿using SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Xml.Linq;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Spec
{
    public class XmlDocReaderTests
    {
        private System.IO.Stream OpenManifestStream(string filename)
        {
            return typeof(XmlDocReaderTests).Assembly.GetManifestResourceStream(typeof(XmlDocReaderTests).Namespace + "." + filename);
        }

        [Fact(DisplayName = "Can_Create_DocReader")]
        public void Can_Create_DocReader()
        {
            var apiName = "Test.Api";

            using (var stream = OpenManifestStream("empty_members.xml"))
            {
                var reader = new DocReader(stream);

                Assert.NotNull(reader);
                Assert.Equal(apiName, reader.AssemblyName);
            }
        }

        [Fact(DisplayName = "Reader_Handles_Missing_Summary_and_Remarks")]
        public void Reader_Handles_Missing_Summary_and_Remarks()
        {
            using (var stream = OpenManifestStream("missing_remarks_summary.xml"))
            {
                var reader = new DocReader(stream);

                Assert.NotNull(reader);

                var member = reader.Members.First();

                Assert.Null(member.Summary);
                Assert.Null(member.Remarks);
            }
        }

        [Fact(DisplayName = "Reader_populates_summary_and_remarks")]
        public void Reader_populates_summary_and_remarks()
        {
            var summaryText = "Summary #1";
            var remarksText = "Remarks #1";

            using (var stream = OpenManifestStream("with_remarks_summary.xml"))
            {
                var reader = new DocReader(stream);

                Assert.NotNull(reader);

                var member = reader.Members.FirstOrDefault();

                Assert.NotNull(member);
                Assert.Equal(summaryText, member.Summary);
                Assert.Equal(remarksText, member.Remarks);
            }
        }

        [Fact(DisplayName = "Reader_Handles_Multiple_Members")]
        public void Reader_Handles_Multiple_Members()
        {
            using (var stream = OpenManifestStream("multiple_elements.xml"))
            {
                var reader = new DocReader(stream);

                Assert.NotNull(reader);

                Assert.Equal(2, reader.Members.Count);
            }
        }

        [Fact(DisplayName = "Reader_Parses_TypeParam_for_Type")]
        public void Reader_Parses_TypeParam_for_Type()
        {
            var paramName = "T";
            var paramText = "The type param for this type";

            using (var stream = OpenManifestStream("generic_class_typeparam.xml"))
            {
                var reader = new DocReader(stream);
                Assert.NotNull(reader);

                var member = reader.Members.FirstOrDefault();
                Assert.NotNull(member);
                Assert.Equal(1, member.TypeParameters.Count);

                var typeParam = member.TypeParameters.FirstOrDefault();
                Assert.NotNull(typeParam);
                Assert.Equal(paramName, typeParam.Name);
                Assert.Equal(paramText, typeParam.Description);
            }
        }

        [Fact(DisplayName = "Reader_understands_see_seealso")]
        public void Reader_understands_see_seealso()
        {
            var ref1 = "T:SomeOtherType";
            var ref2 = "T:AnotherCoolType";

            using (var stream = OpenManifestStream("Reader_understands_see_seealso.xml"))
            {
                var reader = new DocReader(stream);
                Assert.NotNull(reader);
                Assert.Equal(3, reader.Members.Count);

                var foundMember = reader.Members.SingleOrDefault(m => m.Name == "T:SomeType");
                Assert.NotNull(foundMember);
                Assert.Equal(ref1, foundMember.See.First().Cref);
                Assert.Equal(ref2, foundMember.SeeAlso.First().Cref);
            }
        }

        [Fact(DisplayName = "Reader_understands_constant_values")]
        public void Reader_understands_constant_values()
        {
            var memberName = "T:SomeType";
            var summaryText = "This is  **true**  or  **false** .";

            using (var stream = OpenManifestStream("Reader_understands_constant_values.xml"))
            {
                var reader = new DocReader(stream);
                Assert.NotNull(reader);

                var member = reader.Members.FirstOrDefault(m => m.Name == memberName);
                Assert.NotNull(member);
                Assert.Equal(summaryText, member.Summary);
            }
        }

        [Fact]
        public void TempTest()
        {
            var reader = new DocReader(@"C:\bsmith-src\SN.ServiceDiscovery\src\SN.ServiceDiscovery\bin\Debug\SN.ServiceDiscovery.XML");

            List<DocReader> meh = new List<DocReader>();
            meh.Add(reader);

            var t = typeof(TestClasses);
            var interfaces = t.GetInterfaces();
            foreach(var i in interfaces){
                meh.GetDocumentation(i);

                var methods = i.GetMethods().Where(m => m.IsPublic && !m.IsStatic);

                foreach (var meth in methods)
                {
                    var methodDoc = meh.GetMethodDocumentation(meth);
                }
            }

            Assert.NotNull(reader);
        }
    }
}
