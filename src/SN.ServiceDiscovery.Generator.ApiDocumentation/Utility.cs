﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation
{
    public static class Utility
    {
        /// <summary>
        /// Converts the specified <seealso cref="Type"/> to a string representation of its name.
        /// </summary>
        /// <param name="subjectType"></param>
        /// <returns></returns>
        internal static string NamespaceName(Type subjectType)
        {
            if (subjectType.IsNested)
            {
                return NamespaceName(subjectType.DeclaringType) + "+" + subjectType.Name;
            }
            else
            {
                return string.Format("{0}.{1}", subjectType.Namespace, subjectType.Name);
            }
        }

        /// <summary>
        /// Converts the specified <seealso cref="Type"/> to a string representation of its name.
        /// </summary>
        /// <param name="subjectType"></param>
        /// <returns></returns>
        internal static string TypeToName(Type subjectType)
        {
            return NamespaceName(subjectType) + GenericParameterDefinition(subjectType);
        }

        /// <summary>
        /// Constructs the C# style generic type definition for a type
        /// </summary>
        /// <param name="subjectType"></param>
        /// <returns></returns>
        internal static string GenericParameterDefinition(Type subjectType)
        {
            if (subjectType.IsConstructedGenericType)
            {
                var sb = new StringBuilder();
                sb.Append('<');
                var first = true;
                foreach (var p in subjectType.GetGenericArguments())
                {
                    if (!first)
                        sb.Append(',');
                    sb.Append(TypeToName(p));
                    first = false;
                }
                sb.Append('>');
                return sb.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Retrieves the relevant type documentation node from a collection of documentation stores
        /// </summary>
        /// <param name="stores">The documentation collection to query</param>
        /// <param name="toDocument">The type to find in the documentation stores</param>
        /// <returns>A <see cref="XmlDoc.TypeMemberNode"/> if the type could be found, or null otherwise</returns>
        public static XmlDoc.TypeMemberNode GetDocumentation(this IEnumerable<XmlDoc.DocReader> stores, Type toDocument)
        {
            var typeName = toDocument.Name;
            var nmspace = (!string.IsNullOrWhiteSpace(toDocument.Namespace) ? toDocument.Namespace + "." : string.Empty);

            var typeId = string.Format("T:{0}{1}", nmspace, typeName);

            foreach (var store in stores)
            {
                var storeNode = store.GetTypeNode(typeId);
                if (storeNode != null)
                {
                    return storeNode;
                }
            }

            return null;
        }

        private static readonly Regex _friendlyNameParameterSearch = new Regex(@"[(,]\s*([^\s,)]+)", RegexOptions.Compiled);

        /// <summary>
        /// Retrieves the relevant method documentation node from a collection of documentation stores
        /// </summary>
        /// <param name="stores">The documentation collection to query</param>
        /// <param name="endpointInfo">The method metadata to find in the documentation stores</param>
        /// <returns>A <see cref="XmlDoc.MethodMemberNode"/> if the method could be found, or null otherwise</returns>
        public static XmlDoc.MethodMemberNode GetMethodDocumentation(this IEnumerable<XmlDoc.DocReader> stores, System.Reflection.MethodInfo endpointInfo)
        {
            var typeDoc = stores.GetDocumentation(endpointInfo.DeclaringType);
            if (typeDoc != null)
            {
                var methods = typeDoc.Methods.Where(m =>
                    m.MethodParameters.Count == endpointInfo.GetParameters().Length
                    && m.FriendlyName.StartsWith(endpointInfo.Name + "("));

                if (methods.Count() > 1)
                {
                    //retrieve the parameters for the actual method being searched
                    //this will not have references to generic type parameters and all types will be concrete
                    var methodParameterTypes = endpointInfo.GetParameters().Select(p => p.ParameterType.FullName).ToArray();

                    //search over each of the found methods matching the number of parameters
                    foreach (var m in methods)
                    {
                        //extract the parameter types (as strings) from the friendly name
                        var documentedParamList = new List<string>();
                        var matchResults = _friendlyNameParameterSearch.Matches(m.FriendlyName);
                        foreach (Match match in matchResults)
                        {
                            documentedParamList.Add(match.Groups[1].Value);
                        }

                        //sanity check for parameter length
                        if (documentedParamList.Count != methodParameterTypes.Length)
                        {
                            //todo: change to 'continue'
                            throw new InvalidOperationException("method parameter counts out of sync");
                        }

                        //check the lists of paramters against each other
                        bool allParamGood = true;
                        for (var jk = 0; jk < documentedParamList.Count && allParamGood; jk++)
                        {
                            if(documentedParamList[jk] != methodParameterTypes[jk])
                            {
                                string altName = null;

                                if (endpointInfo.DeclaringType.IsGenericType)
                                {
                                    if (typeDoc.TypeParameters.Any(tp => tp.Name == documentedParamList[jk]))
                                    {
                                        altName = endpointInfo.DeclaringType.GenericTypeArguments[typeDoc.TypeParameters.FindIndex(tp => tp.Name == documentedParamList[jk])].FullName;
                                    }
                                }
                                else if (endpointInfo.IsGenericMethod)
                                {
                                    if (m.TypeParameters.Any(tp => tp.Name == documentedParamList[jk]))
                                    {
                                        altName = endpointInfo.GetGenericArguments()[m.TypeParameters.FindIndex(tp => tp.Name == documentedParamList[jk])].FullName;
                                    }
                                }

                                if (altName != methodParameterTypes[jk])
                                {
                                    allParamGood = false;
                                }
                            }
                        }

                        if (allParamGood)
                        {
                            return m;
                        }
                    }
                    //none were found
                    return null;
                }
                //only 1 method, return this one and hope
                return methods.FirstOrDefault();
            }
            //no type documentation was found, guess there are no methods
            return null;
        }

        /// <summary>
        /// Creates a textual representation of a model type
        /// </summary>
        /// <param name="modelType">The type of the model to be output</param>
        /// <param name="parentModels">An optional array types that should not be modeled in detail</param>
        /// <returns>A string representation of the data model</returns>
        public static string GetExampleModel(Type modelType, Type[] parentModels = null)
        {
            //handle system types quickly
            if (modelType.Namespace == "System")
                return string.Format("*{0}*", modelType.FullName);
            
            //make sure parentModels is non-null
            if (parentModels == null)
                parentModels = new Type[0];

            //a list of all models that have been serialized to this point
            var modelHierarchy = parentModels.Concat(new[] { modelType }).ToArray();

            //begin documenting the type by spitting out a nicely formatted version of its name
            StringBuilder modelText = new StringBuilder();
            modelText.AppendFormat("*{0}*", TypeToName(modelType));
            modelText.AppendLine();

            //check to see if the model is an enumerable type
            var isEnumerable = typeof(IEnumerable).IsAssignableFrom(modelType);

            if (isEnumerable)
            {
                //model is some kind of collection

                //determine if it is a generic collection
                var isGeneric = modelType.IsConstructedGenericType;
                if (isGeneric)
                {

                    var genericTypes = modelType.GenericTypeArguments;
                    if (genericTypes.Length == 1)
                    {
                        //generic list
                        modelText.AppendLine("[");
                        if (!parentModels.Contains(genericTypes[0]))
                        {
                            var subModelText = FormatSubModel(GetExampleModel(genericTypes[0], modelHierarchy));
                            modelText.Append("    ");
                            modelText.Append(subModelText);
                        }
                        else
                        {
                            modelText.AppendFormat("    *{0}*", TypeToName(genericTypes[0]));
                        }
                        modelText.AppendLine(",");
                        modelText.AppendLine("    ...");
                        modelText.Append(']');
                    }
                    else if (genericTypes.Length == 2)
                    {
                        //dictionary
                        modelText.AppendLine("{");
                        modelText.AppendLine("  Keys:");
                        modelText.Append("    ");
                        modelText.AppendLine(FormatSubModel(GetExampleModel(genericTypes[0], modelHierarchy)));

                        modelText.AppendLine("  Values:");
                        modelText.Append("    ");
                        modelText.AppendLine(FormatSubModel(GetExampleModel(genericTypes[1], modelHierarchy)));
                        modelText.Append("}");
                    }
                    else
                    {
                        modelText.Append("    ...");
                    }
                }
                else
                {
                    //non-generic enumerable
                    modelText.AppendLine("[");
                    modelText.AppendLine("    { ... },");
                    modelText.AppendLine("    ...");
                    modelText.Append("]");
                }

            }
            else
            {
                //model is an object of some kind
                modelText.AppendLine("{");

                

                //retrieve the properties and fields for the type
                var properties = modelType.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(p => p.CanWrite).ToArray();
                var fields = modelType.GetFields(BindingFlags.Instance | BindingFlags.Public);

                //select the properties and fields into a common format
                var propsAndFields = properties
                    .ToDictionary(pi => (MemberInfo)pi, pi => pi.PropertyType)
                    .Concat(fields.ToDictionary(fi => (MemberInfo)fi, fi => fi.FieldType));

                //iterate over the members
                List<string> members = new List<string>();
                foreach (var member in propsAndFields)
                {
                    members.Add(
                        string.Format(
                            "    {0}: {1}",
                            member.Key.Name,
                            FormatSubModel(GetExampleModel(member.Value, modelHierarchy))
                        )
                    );
                }
                //join the members with commas and newlines
                modelText.AppendLine(string.Join(",\r\n", members));

                modelText.Append('}');
            }

            return modelText.ToString();
        }

        private static string FormatSubModel(string subText)
        {
            return string.Join(
                "\r\n    ",
                subText
                    .Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None));
        }
    }
}