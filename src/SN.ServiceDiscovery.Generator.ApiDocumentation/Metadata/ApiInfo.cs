﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Metadata
{
    public class ApiInfo
    {
        public ApiInfo(ServiceMetadata service, IEnumerable<XmlDoc.DocReader> docStores)
        {
            if (!service.IsValid)
                throw new InvalidOperationException("Cannot construct ApiContainer metadata from an invalid ServiceMetadata.");

            ServiceName = service.ServiceName;
            ServicePath = service.ServicePath;
            IsAuthenticationRequired = service.IsAuthenticationRequired;
            RequiredPrivilege = service.RequiredPrivilege;

            if (string.IsNullOrWhiteSpace(ServiceName))
            {
                ServiceName = Utility.TypeToName(service.ApiDefinitionInterface);
            }

            foreach (var doc in docStores)
            {
                var docInfo = doc.GetTypeNode("T:" + Utility.NamespaceName(service.ApiDefinitionInterface));
                if (docInfo != null)
                {
                    DocSummary = docInfo.Summary;
                    DocRemarks = docInfo.Remarks;
                }
            }

            ImplementedInterfaces = service.AbstractInterfaces
                .OrderBy(t => t == service.ApiDefinitionInterface ? 1 : -1)
                .Select(t=>new InterfaceInfo(t, docStores))
                .ToArray();

            Endpoints = service.Endpoints
                .Where(e => e.IsValid)
                .Select(e => new EndpointInfo(e, docStores))
                .ToArray();
        }

        /// <summary>
        /// Get the display name of the service
        /// </summary>
        public string ServiceName { get; private set; }

        /// <summary>
        /// The summary of this service from the documentation
        /// </summary>
        public string DocSummary { get; private set; }

        /// <summary>
        /// The remarks of this service from the documentation.
        /// </summary>
        public string DocRemarks { get; private set; }

        /// <summary>
        /// Get the path for the service 
        /// </summary>
        public string ServicePath { get; private set; }

        /// <summary>
        /// Returns true if authentication is required for this api
        /// </summary>
        public bool IsAuthenticationRequired { get; private set; }

        /// <summary>
        /// If non-null, specifies a privilege ID that is required to access the api
        /// </summary>
        public string RequiredPrivilege { get; private set; }

        /// <summary>
        /// The list of interfaces that this API exposes
        /// </summary>
        public InterfaceInfo[] ImplementedInterfaces { get; private set; }

        /// <summary>
        /// The list if endpoints in this service
        /// </summary>
        public EndpointInfo[] Endpoints { get; private set; }
    }
}
