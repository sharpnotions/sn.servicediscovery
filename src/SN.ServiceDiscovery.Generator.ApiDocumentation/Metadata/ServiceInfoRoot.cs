﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Metadata
{
    /// <summary>
    /// Represents the root of the serializable documentation cache.
    /// </summary>
    public class ServiceInfoRoot
    {
        /// <summary>
        /// Create a new <seealso cref="ServiceInfoRoot"/> with the specified services and documentation stores.
        /// </summary>
        /// <param name="services">The discovered services to document.</param>
        /// <param name="docStores">The discovered documentation caches.</param>
        public ServiceInfoRoot(IEnumerable<ServiceMetadata> services, IEnumerable<XmlDoc.DocReader> docStores)
        {
            GeneratedOn = DateTimeOffset.Now;
            Services = services.Select(s => new ApiInfo(s, docStores)).ToArray();
        }

        public DateTimeOffset GeneratedOn { get; private set; }

        public ApiInfo[] Services { get; private set; }
    }
}
