﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Metadata
{
    /// <summary>
    /// Information about an interface
    /// </summary>
    public class InterfaceInfo
    {
        public InterfaceInfo(Type interfaceType, IEnumerable<XmlDoc.DocReader> docStores)
        {
            ClassName = Utility.TypeToName(interfaceType);

            var definitionAttribute = interfaceType
                .GetCustomAttributes(typeof(AbstractApiDefinitionAttribute), false)
                .FirstOrDefault() as AbstractApiDefinitionAttribute;

            if (definitionAttribute != null)
            {
                DisplayName = definitionAttribute.Name;
            }

            var doc = docStores.GetDocumentation(interfaceType);
            if (doc != null)
            {
                DocSummary = doc.Summary;
                DocRemarks = doc.Remarks;
            }
        }

        /// <summary>
        /// The display name of the interface
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// The name of the interface, including namespace
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// The summary from the XML documentation
        /// </summary>
        public string DocSummary { get; set; }
        /// <summary>
        /// The remarks from the XML documentation.
        /// </summary>
        public string DocRemarks { get; set; }
    }
}
