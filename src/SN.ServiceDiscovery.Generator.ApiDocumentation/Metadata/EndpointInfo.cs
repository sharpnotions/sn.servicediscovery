﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Metadata
{
    public class EndpointInfo
    {
        public EndpointInfo(ServiceEndpointMetadata endpointMeta, IEnumerable<XmlDoc.DocReader> docStores)
        {
            var doc = docStores.GetMethodDocumentation(endpointMeta.MethodHandle);
            if (doc != null)
            {
                DocSummary = doc.Summary;
                DocRemarks = doc.Remarks;
            }

            var apiMethodReturnType = endpointMeta.MapOutputToType ?? endpointMeta.MethodHandle.ReturnType;

            ReturnType = apiMethodReturnType.FullName;
            if (!apiMethodReturnType.Namespace.StartsWith("System."))
            {
                ReturnTypeModel = Utility.GetExampleModel(apiMethodReturnType);
            }

            Arguments = endpointMeta
                .Parameters
                .Select(p => new ArgumentInfo(p, doc))
                .ToList();
        }



        public string ReturnType { get; private set; }

        public string ReturnTypeModel { get; private set; }

        public string DocSummary { get; private set; }

        public string DocRemarks { get; private set; }

        public List<ArgumentInfo> Arguments { get; private set; }
    }
}
