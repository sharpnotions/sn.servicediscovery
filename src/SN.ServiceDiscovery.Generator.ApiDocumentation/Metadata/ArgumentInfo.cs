﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.Metadata
{
    public class ArgumentInfo
    {
        public ArgumentInfo(ServiceParameterMetadata parameter, XmlDoc.MethodMemberNode methodDoc)
        {
            var argType = parameter.ApiRequestType ?? parameter.ParameterType;
            Name = parameter.ParameterName;
            ArgType = Utility.TypeToName(argType);
            ArgModel = Utility.GetExampleModel(argType);

            if (methodDoc != null)
            {
                var paramDoc = methodDoc.MethodParameters.FirstOrDefault(md => md.Name.Equals(Name));
                if (paramDoc != null)
                {
                    Description = paramDoc.Description;
                }
            }
        }

        public string Name { get; private set; }

        public string ArgType { get; private set; }

        public string ArgModel { get; private set; }

        public string Description { get; private set; }
    }
}
