﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation
{
    /// <summary>
    /// Create a coherent set of documentation from a set of discovered services.
    /// </summary>
    public class ApiDocGenerator
    {
        public ApiDocGenerator(DiscoveryCore core)
        {
            var knownInterfaceAssemblies = core.Services.SelectMany(s => s.AbstractInterfaces.Select(i => i.Assembly)).Distinct();
            var assemblyLocations = GetAssemblyLocations(knownInterfaceAssemblies.Select(a => a.Location));
            var docFiles = GetDocForKnownAssemblies(assemblyLocations);
            var docStores = docFiles.Select(file =>
            {
                using (var stream = file.OpenRead())
                {
                    return new XmlDoc.DocReader(stream);
                }
            });

            var doc = new Metadata.ServiceInfoRoot(core.Services, docStores);


            var outputString = JsonConvert.SerializeObject(doc);
        }

        private IEnumerable<FileInfo> GetAssemblyLocations(IEnumerable<string> paths)
        {
            foreach (var p in paths)
            {
                yield return new FileInfo(p);
            }
        }

        private List<FileInfo> GetDocForKnownAssemblies(IEnumerable<FileInfo> assemblyLocations)
        {
            var discoveredDocumentation = new List<FileInfo>();

            foreach (var a in assemblyLocations)
            {
                if (a.Exists)
                {
                    var assemblyName = a.Name;
                    var shortName = assemblyName.Substring(0, assemblyName.LastIndexOf(a.Extension) - 1);
                    var docName = shortName + ".xml";
                    var docInfo = a.Directory.GetFiles(docName).FirstOrDefault();
                    if (docInfo != null && docInfo.Exists)
                    {
                        discoveredDocumentation.Add(docInfo);
                    }
                }
            }
            return discoveredDocumentation;
        }
    }
}
