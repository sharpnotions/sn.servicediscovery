﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.DocModels
{
    public class MethodDocumentation : DocBase
    {
        public MethodDocumentation()
        {
            Parameters = new List<ParameterDocumentation>();
        }

        public string Description { get; set; }

        public string Remarks { get; set; }

        public bool IsGeneric { get; set; }

        /// <summary>
        /// The return type for this documentaiton
        /// </summary>
        public ParameterDocumentation ReturnType { get; set; }

        /// <summary>
        /// The list of parameters for this API method
        /// </summary>
        public List<ParameterDocumentation> Parameters { get; private set; }
    }
}
