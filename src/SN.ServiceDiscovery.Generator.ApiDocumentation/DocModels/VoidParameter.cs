﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.DocModels
{
    class VoidParameter : ParameterDocumentation
    {
        public VoidParameter()
        {
            Name = "void";
            this.Description = "void return type";
            this.ParameterType = null;
        }

        public override string GetParameterModel()
        {
            return Name;
        }
    }
}
