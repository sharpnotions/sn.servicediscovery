﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.DocModels
{
    /// <summary>
    /// Documentation node for a type (interface, class, abstract class, enum, struct)
    /// </summary>
    public class TypeDocumentation : DocBase
    {
        public TypeDocumentation()
        {
            Methods = new List<MethodDocumentation>();
        }

        public string Summary { get; set; }

        public string Remarks { get; set; }

        public bool IsGeneric { get; set; }

        public List<MethodDocumentation> Methods { get; private set; }
    }
}
