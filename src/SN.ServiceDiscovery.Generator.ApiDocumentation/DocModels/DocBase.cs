﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.DocModels
{
    public abstract class DocBase
    {
        protected DocBase()
        {
            See = new List<ReferenceInfo>();
            SeeAlso = new List<ReferenceInfo>();
        }

        protected string ParseReferences(string plainText)
        {
            return plainText;
        }


        public List<ReferenceInfo> See { get; private set; }

        public List<ReferenceInfo> SeeAlso { get; private set; }
    }
}
