﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.DocModels
{
    /// <summary>
    /// An information class for references
    /// </summary>
    public class ReferenceInfo
    {
        /// <summary>
        /// The class or method name for a reference
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// The referential link to find the item in the documentation cache
        /// </summary>
        public string DocReferenceName { get; set; }
    }
}
