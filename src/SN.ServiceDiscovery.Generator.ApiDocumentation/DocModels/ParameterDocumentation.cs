﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.DocModels
{
    /// <summary>
    /// Documentation node for a method parameter
    /// </summary>
    public class ParameterDocumentation : DocBase
    {
        /// <summary>
        /// The name of the parameter
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description for the parameter
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The type of the parameter
        /// </summary>
        public Type ParameterType { get; set; }

        /// <summary>
        /// Gets a JSON string for the <see cref="ParameterType"/> model.
        /// </summary>
        /// <returns>A JSON string</returns>
        public virtual string GetParameterModel()
        {
            return JsonConvert.SerializeObject(Activator.CreateInstance(ParameterType), Formatting.Indented);
        }
    }
}
