﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// Represents a Class/Interface level type of code construct
    /// </summary>
    public abstract class MemberNode : WithReferences
    {
        private static readonly Regex _genericTypePattern = new Regex(@"(`{1,2})(\d+)", RegexOptions.Compiled);
        private List<ParameterNode> _typeParameters = new List<ParameterNode>();

        /// <summary>
        /// Create a new <see cref="MemberNode"/> object.
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="nodeSource"></param>
        public MemberNode(string nodeName, XElement nodeSource)
        {
            SetName(nodeName);

            ProcessField("summary", nodeSource.Elements("summary").FirstOrDefault());

            ProcessField("remarks", nodeSource.Elements("remarks").FirstOrDefault());

            TypeParameters.AddRange(
                nodeSource
                    .Elements("typeparam")
                    .Select(n => new ParameterNode(n))
            );
        }

        /// <summary>
        /// The documentation tag that identifies this member uniquely.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The namespace of the member
        /// </summary>
        public string Namespace { get; private set; }

        /// <summary>
        /// The friendly name of this member.
        /// </summary>
        /// <remarks>This property is derived by processing the member name and any necessary type parameters.</remarks>
        public string FriendlyName { get; private set; }

        /// <summary>
        /// The summary documentation for this member node.
        /// </summary>
        public string Summary { get; private set; }

        /// <summary>
        /// The remarks documentation for this member node
        /// </summary>
        public string Remarks { get; private set; }

        /// <summary>
        /// The list of type parameters for this member
        /// </summary>
        public List<ParameterNode> TypeParameters { get { return _typeParameters; } }

        /// <summary>
        /// Collects references from all parameter entries into this member entry
        /// </summary>
        public virtual void CollectReferences()
        {
            foreach (var tp in TypeParameters)
            {
                See.AddRange(tp.See.Where(r => !See.Any(r2 => r2.Equals(r))));
                SeeAlso.AddRange(tp.SeeAlso.Where(r => !SeeAlso.Any(r2 => r2.Equals(r))));
            }
        }

        /// <summary>
        /// Processes any generic references in the FriendlyName property
        /// </summary>
        public void ProcessGenericReferences()
        {
            if (FriendlyName.Contains('`'))
            {
                var newFriendly = _genericTypePattern.Replace(FriendlyName, ReplaceGenericReference);
                FriendlyName = newFriendly;
            }
        }

        /// <summary>
        /// Replaces the generic reference in the regex match
        /// </summary>
        /// <param name="m">The regex match with 2 capturing groups: (1) the back-ticks, (2) the digit</param>
        /// <returns>A string with the replacement text</returns>
        public abstract string ReplaceGenericReference(Match m);

        /// <summary>
        /// Sets the <see cref="Name"/> and <see cref="FriendlyName"/> properties by processing the member's name attribute.
        /// </summary>
        /// <param name="name">The name attribute from the XML Documentation.</param>
        protected void SetName(string name)
        {
            Name = name;
            var temp = name.Skip(2);
            var nameBuffer = new StringBuilder();
            var namespaceBuffer = new StringBuilder();
            var consumeRemaining = false;
            foreach (var nameChar in temp)
            {
                if (consumeRemaining)
                {
                    nameBuffer.Append(nameChar);
                }
                else
                {
                    if (nameChar == '.')
                    {
                        namespaceBuffer.Append(nameBuffer);
                        namespaceBuffer.Append('.');
                        nameBuffer.Length = 0;
                    }
                    else
                    {
                        if (nameChar == '(')
                            consumeRemaining = true;
                        nameBuffer.Append(nameChar);
                    }
                }
            }
            FriendlyName = nameBuffer.ToString();
            Namespace = namespaceBuffer.ToString().TrimEnd(new[] { '.' });
        }

        protected string GetTypeParameterName(string referenceIndex)
        {
            int tpIndex;
            if (int.TryParse(referenceIndex, out tpIndex))
            {
                if (tpIndex >= TypeParameters.Count)
                {
                    //when the type parameter index reference specifies an index
                    //equal to the number of type parameters, it is looking for
                    //the generic declaration
                    return "<" + string.Join(", ", TypeParameters.Select(p => p.Name)) + ">";
                }
                else
                {
                    //when the type parameter index is less than the number
                    //of type parameters, it is looking for the type parameter's name
                    return TypeParameters[tpIndex].Name;
                }
            }
            return null;
        }

        /// <summary>
        /// Handles setting property and field values after they have been processed by the base class
        /// embedded reference text processor.
        /// </summary>
        /// <param name="fieldName">The <see cref="String"/> name of the field.</param>
        /// <param name="fieldValue">The <see cref="String"/> value of the field.</param>
        protected override void SetField(string fieldName, string fieldValue)
        {
            switch (fieldName)
            {
                case "summary": Summary = fieldValue; break;
                case "remarks": Remarks = fieldValue; break;
                default: throw new InvalidOperationException("Unknown fieldName: " + fieldName);
            }
        }
    }
}
