﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// An XML documentation reader, which acts as a catalog of members found within the XML documentation
    /// </summary>
    public class DocReader
    {
        private List<MemberNode> _members = new List<MemberNode>();

        /// <summary>
        /// Creates a new <see cref="DocReader"/> instance by opening the XML file at the specified path.
        /// </summary>
        /// <param name="path">The path to the XML document.</param>
        public DocReader(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open))
            {
                ReadDoc(stream);
            }
        }

        /// <summary>
        /// Creates a new <see cref="DocReader"/> instance by reading the stream.
        /// </summary>
        /// <param name="incoming">The incoming stream of XML.</param>
        public DocReader(Stream incoming)
        {
            ReadDoc(incoming);
        }

        /// <summary>
        /// The members discovered in the xml documentation
        /// </summary>
        public List<MemberNode> Members { get { return _members; } }

        /// <summary>
        /// The name of the assembly discovered from the documentation
        /// </summary>
        public string AssemblyName { get; private set; }

        /// <summary>
        /// Temporary location of serialized documentation. TODO: REMOVE
        /// </summary>
        public string SerializedDoc { get; private set; }

        /// <summary>
        /// Determines if the specified identifier represents a type
        /// </summary>
        /// <param name="identifier">The string identifier</param>
        /// <returns><c>true</c> if the identifier is for a Type</returns>
        public bool IsTypeIdentifier(string identifier)
        {
            return (identifier ?? string.Empty).StartsWith("T:");
        }

        /// <summary>
        /// Determines if the specified identifier represents a method
        /// </summary>
        /// <param name="identifier">The string identifier</param>
        /// <returns><c>true</c> if the identifier is for a Method</returns>
        public bool IsMethodIdentifier(string identifier)
        {
            return (identifier ?? string.Empty).StartsWith("M:");
        }

        /// <summary>
        /// Retrieves any <seealso cref="MemberNode"/> which matches the given identifier.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public MemberNode GetMemberNode(string identifier)
        {
            return Members.FirstOrDefault(m => m.Name.Equals(identifier, StringComparison.Ordinal));
        }

        /// <summary>
        /// Retrieves a <seealso cref="TypeMemberNode"/> for the requested identifier
        /// </summary>
        /// <param name="identifier">The identifier for the type</param>
        /// <returns>the MemberNode, or null if the identifier could not be found</returns>
        public TypeMemberNode GetTypeNode(string identifier)
        {
            if (IsTypeIdentifier(identifier))
            {
                return Members.Where(m => m.Name.Equals(identifier, StringComparison.Ordinal))
                    .OfType<TypeMemberNode>()
                    .FirstOrDefault();
            }
            return null;
        }

        private void ReadDoc(Stream incoming)
        {
            var xmlDocument = XDocument.Load(incoming);
            var docElement = xmlDocument.Root;

            AssemblyName = docElement.Element("assembly").Element("name").Value;

            var members = docElement.Element("members").Elements("member");

            foreach (var m in members)
            {
                ReadMember(m);
            }

            //members have been loaded. Process them.
            MembersLoaded();

            //todo: remove temporary serialization
            SerializedDoc = JsonConvert.SerializeObject(Members.Where(m=>m is TypeMemberNode || (m is MethodMemberNode && ((MethodMemberNode)m).OwnerType == null)), new JsonSerializerSettings { Formatting = Newtonsoft.Json.Formatting.Indented });
        }

        private void ReadMember(XElement member)
        {
            var memberNodeName = member.Attribute("name").Value;
            if (memberNodeName.StartsWith("T:"))
            {
                var typeNode = new TypeMemberNode(memberNodeName, member);
                _members.Add(typeNode);
            }
            else if (memberNodeName.StartsWith("M:"))
            {
                _members.Add(new MethodMemberNode(memberNodeName, member));
            }
            else if (memberNodeName.StartsWith("F:"))
            {
                //field, don't care
            }
            else if(memberNodeName.StartsWith("P:"))
            {
                //property, don't care
            }
        }

        private void MembersLoaded()
        {
            CollectReferences();
            RegisterMethods();
            ExpandGenericNames();
            RetrieveFriendlyNamesForReferences();
            TrimAssociatedMethods();
        }

        private void CollectReferences()
        {
            foreach (var m in Members)
            {
                m.CollectReferences();
            }
        }

        private void RegisterMethods()
        {
            foreach (var m in Members.OfType<MethodMemberNode>().ToArray())
            {
                var typeId = "T:" + m.Namespace;
                var typeNode = GetTypeNode(typeId);
                if (typeNode == null)
                {
                    typeNode = new TypeMemberNode(
                        typeId,
                        new XElement("member",
                            new XAttribute("name", typeId))
                    );
                    Members.Add(typeNode);

                    System.Diagnostics.Trace.TraceWarning("Could not find typeId " + typeId + " for method " + m.Name);
                }
                typeNode.RegisterMethod(m);
            }
        }

        private void ExpandGenericNames()
        {
            foreach (var m in Members)
            {
                m.ProcessGenericReferences();
            }
        }

        private void RetrieveFriendlyNamesForReferences()
        {
            foreach (var reference in Members.SelectMany(m => m.See).Concat(Members.SelectMany(m => m.SeeAlso)))
            {
                if (string.IsNullOrWhiteSpace(reference.FriendlyName))
                {
                    if (string.IsNullOrWhiteSpace(reference.AltText))
                    {
                        if (reference.Cref.StartsWith("!:"))
                        {
                            //invalid documentation reference
                            reference.FriendlyName = "~" + reference.Cref.Substring(2) + "~";
                        }
                        else
                        {
                            var foundMember = GetMemberNode(reference.Cref);
                            if (foundMember == null)
                            {
                                System.Diagnostics.Trace.TraceWarning("Reference Type not found for " + reference.Cref);
                                reference.FriendlyName = reference.Cref.Substring(2);
                            }
                            else
                            {
                                reference.FriendlyName = foundMember.FriendlyName;
                            }
                        }
                    }
                    else
                    {
                        reference.FriendlyName = reference.AltText;
                    }
                }
            }
        }

        private void TrimAssociatedMethods()
        {
            Members.RemoveAll(n => n is MethodMemberNode && ((MethodMemberNode)n).OwnerType != null);
        }
    }
}
