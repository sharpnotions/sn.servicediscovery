﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// A node that can represent a TypeParameter or a Method parameter.
    /// </summary>
    public class ParameterNode : WithReferences
    {
        /// <summary>
        /// Create a new <see cref="ParameterNode"/> from the source XML.
        /// </summary>
        /// <param name="nodeSource">The source XML node</param>
        public ParameterNode(XElement nodeSource)
        {
            if (nodeSource == null) throw new ArgumentNullException("nodeSource");

            Name = nodeSource.Attribute("name").Value;
            ProcessField("description", nodeSource);
        }

        /// <summary>
        /// The name of the parameter in code
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The description text from the source XML.
        /// </summary>
        public string Description { get; set; }

        protected override void SetField(string fieldName, string fieldValue)
        {
            switch (fieldName)
            {
                case "description": Description = fieldValue; break;
                default: throw new InvalidOperationException("Unknown fieldName: " + fieldName);
            }
        }
    }
}