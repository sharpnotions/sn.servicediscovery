﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// Represents a Class/Interface level type of code construct
    /// </summary>
    public class TypeMemberNode : MemberNode
    {
        private List<MethodMemberNode> _methods = new List<MethodMemberNode>();

        /// <summary>
        /// Create a new <see cref="TypeMemberNode"/> object.
        /// </summary>
        /// <param name="nodeName">The name of the node</param>
        /// <param name="nodeSource">The xml source node</param>
        public TypeMemberNode(string nodeName, XElement nodeSource)
            : base(nodeName, nodeSource)
        {

        }

        /// <summary>
        /// Retrieves the list of Methods found in this Type
        /// </summary>
        public List<MethodMemberNode> Methods { get { return _methods; } }

        /// <summary>
        /// Registers a method in this type node, and creates a link from the method to this node as well.
        /// </summary>
        /// <param name="method">The method to register</param>
        public void RegisterMethod(MethodMemberNode method)
        {
            if (method == null) return;
            Methods.Add(method);
            method.OwnerType = this;
        }

        /// <summary>
        /// Replaces the generic reference in the regex match
        /// </summary>
        /// <param name="m">The regex match with 2 capturing groups: (1) the back-ticks, (2) the digit</param>
        /// <returns>A string with the replacement text</returns>
        public override string ReplaceGenericReference(System.Text.RegularExpressions.Match m)
        {
            switch (m.Groups[1].Length)
            {
                case 1: break;
                default:
                    throw new InvalidOperationException("Cannot process regex match with " + m.Groups[1].Length + " back-ticks.");
            }

            return GetTypeParameterName(m.Groups[2].Value) ?? m.Groups[0].Value;
        }
    }
}
