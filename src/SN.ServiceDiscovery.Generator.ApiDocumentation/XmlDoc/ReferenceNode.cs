﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// Represents a reference to another documented object within the documentation hierarchy.
    /// </summary>
    public class ReferenceNode
    {
        /// <summary>
        /// Create a new <see cref="ReferenceNode"/> instance with the specified code reference and alt text.
        /// </summary>
        /// <param name="cref">The value for the <seealso cref="Cref"/>.</param>
        /// <param name="altText">The value for the <seealso cref="AltText"/>.</param>
        public ReferenceNode(string cref, string altText)
        {
            Cref = cref;
            AltText = altText;
            ProcessFriendlyName();
        }

        private void ProcessFriendlyName()
        {
            if (!string.IsNullOrWhiteSpace(AltText))
            {
                FriendlyName = AltText;
            }
            else
            {
                FriendlyName = null;
            }
        }

        /// <summary>
        /// The code reference name for this link. Corresponds to <seealso cref="MemberNode.Name"/>
        /// </summary>
        public string Cref { get; private set; }

        /// <summary>
        /// The alternative text supplied by the contents of the reference tag's text nodes.
        /// </summary>
        public string AltText { get; private set; }

        /// <summary>
        /// The friendly name that can be output to the end user
        /// </summary>
        public string FriendlyName { get; set; }

        /// <summary>
        /// Checks to see if two instances have the same Cref field.
        /// </summary>
        /// <param name="obj">The object to check</param>
        /// <returns><c>true</c> if the Cref fields match.</returns>
        public override bool Equals(object obj)
        {
            if ((obj as ReferenceNode) == null) return false;
            var rnode = (ReferenceNode)obj;
            return (rnode.Cref ?? string.Empty).Equals(Cref, StringComparison.Ordinal);
        }

        /// <summary>
        /// Retrieves the hash code of this instance
        /// </summary>
        /// <returns>The integer hash code</returns>
        public override int GetHashCode()
        {
            return (Cref != null ? Cref.GetHashCode() : 0)
                ^ (AltText != null ? AltText.GetHashCode() : 0)
                ^ (FriendlyName != null ? FriendlyName.GetHashCode() : 0);
        }

        /// <summary>
        /// Checks to see if two instances of <seealso cref="ReferenceNode"/> have the same Cref
        /// </summary>
        /// <param name="a">The first ReferenceNode object</param>
        /// <param name="b">The second ReferenceNode object</param>
        /// <returns><c>true</c> if the instances point to the same reference.</returns>
        public static bool operator ==(ReferenceNode a, ReferenceNode b)
        {
            var oa = a as object;
            var ob = b as object;
            
            if (oa == null && ob == null) return true;
            if (oa != null && ob != null) return a.Equals(b);
            return false;
        }

        /// <summary>
        /// Checks if two reference nodes are not equal.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(ReferenceNode a, ReferenceNode b)
        {
            var oa = a as object;
            var ob = b as object;

            if (oa == null && ob == null) return false;
            if (oa != null && ob != null) return !a.Equals(b);
            return true;
        }
    }
}
