﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// Provides a building-block for nodes which may contain references to other member-nodes within the documentation hierarchy.
    /// </summary>
    public abstract class WithReferences
    {
        private List<ReferenceNode> _see = new List<ReferenceNode>();
        private List<ReferenceNode> _seeAlso = new List<ReferenceNode>();

        /// <summary>
        /// The list of references which have been marked as "see" resources.
        /// </summary>
        public List<ReferenceNode> See { get { return _see; } }

        /// <summary>
        /// The list of references which have been marked as "see also" resources.
        /// </summary>
        public List<ReferenceNode> SeeAlso { get { return _seeAlso; } }

        /// <summary>
        /// Catalogs any references found in the parentNode and passes a textual representation of the contents to <seealso cref="SetField"/>
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="parentNode"></param>
        protected void ProcessField(string fieldName, XElement parentNode)
        {
            StringBuilder sb = new StringBuilder();

            if (parentNode != null)
            {
                IEnumerable<XNode> nodes = parentNode.Nodes();

                foreach (var item in nodes)
                {
                    if (item is XText)
                    {
                        SpaceAppend(sb, ((XText)item).Value);
                    }
                    else if (item is XElement)
                    {
                        var element = (XElement)item;
                        if (element.Name.LocalName == "see" || element.Name.LocalName == "seealso")
                        {
                            var refnode = new ReferenceNode(element.Attribute("cref").Value, element.Value);
                            SpaceAppend(sb, "{{" + refnode.Cref + "}}");
                            if (element.Name.LocalName == "see")
                            {
                                _see.Add(refnode);
                            }
                            else
                            {
                                _seeAlso.Add(refnode);
                            }
                        }
                        else if (element.Name.LocalName == "c")
                        {
                            //constant value
                            SpaceAppend(sb, "**" + element.Value + "**");
                        }
                    }
                }
            }

            var fieldValue = sb.ToString().Trim();
            if (string.IsNullOrWhiteSpace(fieldValue))
            {
                fieldValue = null;
            }

            SetField(fieldName, fieldValue);
        }

        /// <summary>
        /// Appends a space to the <seealso cref="StringBuilder"/> if the stringbuilder has anything in it.
        /// </summary>
        /// <param name="sb">The <seealso cref="StringBuilder"/> to work with.</param>
        /// <param name="toAppend">The string to append to the builder.</param>
        private void SpaceAppend(StringBuilder sb, string toAppend)
        {
            if (!string.IsNullOrWhiteSpace(toAppend))
            {
                if (sb.Length > 0) sb.Append(' ');
                sb.Append(toAppend);
            }
        }

        /// <summary>
        /// Set a field within an implementing class to the processed value
        /// </summary>
        /// <param name="fieldName">The name of the field to set</param>
        /// <param name="fieldValue">The value of the field</param>
        protected abstract void SetField(string fieldName, string fieldValue);
    }
}
