﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace SN.ServiceDiscovery.Generator.ApiDocumentation.XmlDoc
{
    /// <summary>
    /// Represents a method node from the XML documentation
    /// </summary>
    public class MethodMemberNode : MemberNode
    {
        private List<ParameterNode> _methodParameters = new List<ParameterNode>();

        /// <summary>
        /// Create a new <see cref="MethodMemeberNode"/> by processing the node's name and source XML node.
        /// </summary>
        /// <param name="nodeName">The name identifier for the member node.</param>
        /// <param name="nodeSource">The source XML node for the method</param>
        public MethodMemberNode(string nodeName, XElement nodeSource)
            : base(nodeName, nodeSource)
        {
            MethodParameters.AddRange(
                nodeSource
                    .Elements("param")
                    .Select(n => new ParameterNode(n))
            );
            ProcessField("returns", nodeSource.Elements("returns").FirstOrDefault());
        }

        /// <summary>
        /// The list of method parameters
        /// </summary>
        public List<ParameterNode> MethodParameters { get { return _methodParameters; } }

        /// <summary>
        /// The return type of the method
        /// </summary>
        public string Returns { get; private set; }

        /// <summary>
        /// The owning <see cref="TypeMemberNode"/> for this method
        /// </summary>
        [JsonIgnore]
        public TypeMemberNode OwnerType { get; set; }

        /// <summary>
        /// Gets <c>true</c> if this method is a constructor method
        /// </summary>
        public bool IsConstructor
        {
            get
            {
                return FriendlyName.StartsWith("#ctor");
            }
        }

        protected override void SetField(string fieldName, string fieldValue)
        {
            if (fieldName == "returns")
            {
                Returns = fieldValue;
            }
            else
            {
                base.SetField(fieldName, fieldValue);
            }
        }

        /// <summary>
        /// Collects references from all parameter entries into this member entry
        /// </summary>
        public override void CollectReferences()
        {
            base.CollectReferences();

            foreach (var mp in MethodParameters)
            {
                See.AddRange(mp.See.Where(r => !See.Any(r2 => r2.Equals(r))));
                SeeAlso.AddRange(mp.SeeAlso.Where(r => !SeeAlso.Any(r2 => r2.Equals(r))));
            }
        }

        /// <summary>
        /// Replaces the generic reference in the regex match
        /// </summary>
        /// <param name="m">The regex match with 2 capturing groups: (1) the back-ticks, (2) the digit</param>
        /// <returns>A string with the replacement text</returns>
        public override string ReplaceGenericReference(System.Text.RegularExpressions.Match m)
        {
            switch (m.Groups[1].Length)
            {
                case 1: return OwnerType.ReplaceGenericReference(m);
                case 2: break;
                default:
                    throw new InvalidOperationException("Cannot process regex match with " + m.Groups[1].Length +" back-ticks.");
            }
            return GetTypeParameterName(m.Groups[2].Value) ?? m.Groups[0].Value;
        }
    }
}
