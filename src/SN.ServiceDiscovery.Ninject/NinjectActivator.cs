﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Parameters;

namespace SN.ServiceDiscovery.NinjectActivator
{
    /// <summary>
    /// A ninject activator implementation of <see cref="IServiceActivator"/>
    /// </summary>
    public class NinjectServiceActivator : IServiceActivator, IDisposable
    {
        private bool disposed = false;
        private IKernel Kernel;

        /// <summary>
        /// Create a new instance of <see cref="NinjectServiceActivator"/> with the specified kernel.
        /// </summary>
        /// <param name="kernel">The ninject kernel to use when activating services</param>
        public NinjectServiceActivator(IKernel kernel)
        {
            Kernel = kernel;
        }

        /// <summary>
        /// Activates an instance of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of service to activate.</typeparam>
        /// <param name="activationParameters">The parameters to pass to ninject during activation</param>
        /// <returns>The activated service</returns>
        public T Activate<T>(params Object[] activationParameters)
        {
            IParameter[] parameters = null;
            if (activationParameters != null)
            {
                parameters = activationParameters.OfType<IParameter>().ToArray();
            }

            return Kernel.Get<T>(parameters);
        }

        /// <summary>
        /// Activates an instance of the specified type.
        /// </summary>
        /// <param name="serviceType">The type of service to activate</param>
        /// <param name="activationParameters">Any parameters that need to be passed to Ninject during activation</param>
        /// <returns>The activated service</returns>
        public object Activate(Type serviceType, params object[] activationParameters)
        {
            if (serviceType == null) throw new ArgumentNullException("serviceType");
            IParameter[] parameters = null;
            if(activationParameters != null)
            {
                parameters = activationParameters.OfType<IParameter>().ToArray();
            }
            return Kernel.Get(serviceType, parameters);
        }

        /// <summary>
        /// Disposes of the object by releasing external resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                Kernel = null;
            }
            disposed = true;
        }
    }
}
