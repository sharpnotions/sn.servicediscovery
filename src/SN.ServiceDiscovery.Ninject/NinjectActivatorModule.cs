﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.ServiceDiscovery.NinjectActivator
{
    public class NinjectActivatorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IServiceActivator>().ToConstant<NinjectServiceActivator>(new NinjectServiceActivator(this.Kernel));
            Bind<DiscoveryCore>().ToMethod(ctx =>
            {
                //discover classes in the current application domain that have the ApiImplementationAttribute
                var knownServices = AppDomain.CurrentDomain
                    .GetAssemblies()
                    .Where(a => !a.IsDynamic)
                    .SelectMany(a => a.GetTypes()
                        .Where(t => t.IsClass
                                    && !t.IsAbstract
                                    && t.GetCustomAttributes(false).Any(att => att is ApiImplementationAttribute)))
                    .Distinct()
                    .ToArray();

                //create a new discovery core with the configured activator, typemapper, and list of known services
                return new DiscoveryCore(ctx.Kernel.Get<DiscoveryConfiguration>(), knownServices);
            });
        }
    }
}
